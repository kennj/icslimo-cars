import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-comfirmed',
  templateUrl: './comfirmed.component.html',
  styleUrls: ['./comfirmed.component.scss']
})
export class ComfirmedComponent implements OnInit {
content = 'Thank you for booking with us. You will receive a confirmation email shortly Have a good trip !';
redirect_url = '/';
  constructor() { }

  ngOnInit(): void {
  }

}
