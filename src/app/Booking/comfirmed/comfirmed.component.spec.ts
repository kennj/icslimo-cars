import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComfirmedComponent } from './comfirmed.component';

describe('ComfirmedComponent', () => {
  let component: ComfirmedComponent;
  let fixture: ComponentFixture<ComfirmedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComfirmedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComfirmedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
