import { typeWithParameters } from '@angular/compiler/src/render3/util';
import { Component, OnInit, Input, Output,  EventEmitter, HostListener } from '@angular/core';
declare var airlines;
declare var countries;
// declare var airports;
// declare var seaports;
@Component({
  selector: 'app-passenger-information',
  templateUrl: './passenger-information.component.html',
  styleUrls: ['./passenger-information.component.scss']
})
export class PassengerInformationComponent implements OnInit {
  @Input() Reservation: any;
  @Input() car: any;
  @Input() Passenger_Info: any;
  @Input() PickUpFromField: any;
  @Output() GetData = new EventEmitter();
  @Output() GetFieldData = new EventEmitter();
  @Output() Back = new EventEmitter();
  Date = new Date();

   // Data Pick Up Type
   PickUpType = {
    PTP:'Point to Point',
    HR:'Hourly',
    DL:'Daily',
   };
   PassengerInformation = {
    FirstName:'',
    LastName:'',
    EmailAddress:'',
    ContactNumber:'',
    IATANAccount:'',
    FullName:'',
    EmailAddressBooker:'',
    ContactNumberBooker:'',
    UnitedMileageAccount:'',
    RewardsOptional:'',
   };

   options = [
    {code:'test',label:'Data Test - US - UNITED STATES'}, 
    {code:'test',label:'Data Test - AF - AFGHANISTAN'}, 
    {code:'test',label:'Data Test - CA - CANADA'}, 
    {code:'test',label:'Data Test - IL - ISRAEL'}, 
    {code:'test',label:'Data Test - East New York (Brooklyn) NY'}, 
    {code:'test',label:'Data Test - New jersey, New jersey'}, 
    {code:'test',label:'Data Test - California  MD'}, 
    {code:'test',label:'Data Test - WRI - McGuire Air Force Base, Trenton, New Jersey'}, 
    {code:'test',label:'Data Test - MHT - Manchester–Boston Regional Airport'}, 
   ];
  filteredOptionsFrom:any;


   Check_form = false;

  constructor() { }

  ngOnInit(): void {
    if(this.Passenger_Info.EmailAddress){
      this.PassengerInformation = this.Passenger_Info;
    }
  }

  check_forms(){
    this.Check_form = true;
    const error = [];
    const check = [
    'FirstName',
    'LastName',
    'ContactNumber',
    'IATANAccount',
    'FullName',
    'ContactNumberBooker',
    ];
    for(var i of check){
      if(!this.PassengerInformation[i])error.push(i);
    }
    if(!this.isEmail(this.PassengerInformation['EmailAddress'])){
      error.push('EmailAddress');
    }
    if(!this.isEmail(this.PassengerInformation['EmailAddressBooker'])){
      error.push('EmailAddressBooker');
    }

    for(var field of this.PickUpFromField.From[this.Reservation.PickUpFrom]){
      if(!field.value && field.required )error.push(i);
    }
    for(var field of this.PickUpFromField.To[this.Reservation.PickUpTo]){
      if(!field.value && field.required )error.push(i);
    }

    console.log(error);
    if(error.length<1){
      this.PassengerInformation['step_next'] = 4;
      this.GetData.emit(this.PassengerInformation);
      this.GetFieldData.emit(this.PickUpFromField);
    }
  }

  UpdateBack(s){
    this.Back.emit(s);
  }

  ChangeEvent(name,event){
    this.PassengerInformation[name] = event.target.value;
  }


  isEmail(email:string)
  {
    const emailRegexp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return emailRegexp.test(email);
  }

  ChangeField(s,name,event){
    this.PickUpFromField[name][s].value = event.target.value;
  }

  private _filter(item,value: string){
    var data =  this.options;
    const filterValue = value.toLowerCase();
    if(item.type == 'countries')data = countries;
    if(item.type == 'airlines')data = airlines;
    return data.filter(option => option.label.toLowerCase().includes(filterValue));
  }

  ChangeFormPickUp( s,type,name:string,event){
    if(this.PickUpFromField[type][name][s]['type']){
      this.filteredOptionsFrom = this._filter(this.PickUpFromField[type][name][s],event.target.value);
    }else{
      this.filteredOptionsFrom = [];
    }

    this.PickUpFromField[type][name][s].value= event.target.value;
    console.log(this.filteredOptionsFrom);
  }
  SelectAutocomplete( s,type,name:string, event) {
    console.log(event.option.viewValue);
    this.PickUpFromField[type][name][s].value= event.option.viewValue;
    this.filteredOptionsFrom = [];
  }

}
