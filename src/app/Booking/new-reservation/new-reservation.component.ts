import { Component, OnInit, Input, Output,  EventEmitter, HostListener } from '@angular/core';
import { Sort } from '@angular/material/sort';
import {FormGroup,FormControl} from '@angular/forms';
import {MatDatepicker} from '@angular/material/datepicker';

import {Observable} from 'rxjs';
import {startWith, map} from 'rxjs/operators';

@Component({
  selector: 'app-new-reservation',
  templateUrl: './new-reservation.component.html',
  styleUrls: ['./new-reservation.component.scss']
})
export class NewReservationComponent implements OnInit {
@Input() DataItem: any;
@Input() PickUpFromData: any;
@Output() GetData = new EventEmitter();

    // Data Pick Up Type
    PickUpType = [
      {name:'PTP',option:'Point to Point'},
      {name:'HR',option:'Hourly',value:'',required:true},
      {name:'DL',option:'Daily'},
    ];

    // Support
    Supports = [
      {name:'pet',label:'I have a pet riding with me',checked:false},
      {name:'booster',label:'I need a booster',checked:false},
      {name:'car_seat',label:'I need a car seat',checked:false},
      {name:'infant',label:'I need an infant car seat',checked:false},
    ];

      // Add Stops
      AddStops = [];


   Check_form = false;
   date = new Date(); 
   PickDateDefault : FormGroup;
   constructor() { }

   options: string[] = [
     'US - UNITED STATES', 
     'AF - AFGHANISTAN', 
     'CA - CANADA',
     'IL - ISRAEL',
     'East New York (Brooklyn) NY', 
     'New jersey, New jersey', 
     'California  MD', 
     'WRI - McGuire Air Force Base, Trenton, New Jersey', 
     'MHT - Manchester–Boston Regional Airport'];
   filteredOptionsFrom:any;

   Reservation = {
    PickUpDate: this.date,
    PickUpTime:'05:30 pm',
    PickUpFrom:'airport',
    PickUpTo:'address',
    PickUpType:'PTP',
    Luggage: 1,
   Passenger: 1,
   PickUpTypeHours:'',
   Supports:this.Supports
  };

  ngOnInit(): void {
    if(this.DataItem.PickUpDate){
      this.Reservation = this.DataItem;
      this.AddStops = this.DataItem.AddStops;
      this.Supports = this.DataItem.Supports;
      this.PickDateDefault = new FormGroup({
        date:new FormControl(this.DataItem.PickUpDate),
      });
    }

    this.PickDateDefault = new FormGroup({
      date:new FormControl(this.date),
    });

    
  }



  
 // save field when change
   DateEvent( name:string, event) {
     this.Reservation[name]= event.value;
   }
   InputEvent( name:string, event) {
    this.Reservation[name]= event.target.value;
  }
  SupportEvent(i,name,event){
    this.Reservation.Supports[i]['checked']=event.target.checked;
  }
  timeChanged(event){
    this.Reservation['PickUpTime']= event;
  }
   PickUpEvent(name:string, event){
    this.Reservation[name]= event.target.value;
   }


   filter_data(data,key,value) {
    const result = data.filter(S => S[key].includes(value));
     return (result)?result[0]:'';
  }

  AddNewStops(){
    const item = {address:'',Route:false,EstMinutes:20};
    if(this.AddStops.length < 3)this.AddStops.push(item);
  }
  DeleteStops(s){
   this.AddStops.splice(s,1);
  }
  AddStopEvent(s,name,event){
    this.AddStops[s][name] = event.target.value;
    if(name=='Route')this.AddStops[s][name]=event.target.checked;
  }
  check_forms(){
    this.Check_form = true;
    const error = [];
    const check = [
      'PickUpDate', 
      'PickUpTime',
      'PickUpFrom',
      'PickUpTo',
    ];
    for(var i of check){
      if(!this.Reservation[i])error.push(i);
    }
    if( !this.Reservation['PickUpTypeHours']&& this.Reservation['PickUpType'] == 'HR'){
      error.push('Hourly is required !');
    }
    console.log(error);
    this.Reservation['AddStops'] = this.AddStops;
    if(error.length<1){
      this.Reservation['step_next'] = 2;
      this.GetData.emit(this.Reservation);
    }

  }






}
