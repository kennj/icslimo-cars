import { Component, OnInit, Input, Output,  EventEmitter, HostListener } from '@angular/core';

@Component({
  selector: 'app-summarize-payment',
  templateUrl: './summarize-payment.component.html',
  styleUrls: ['./summarize-payment.component.scss']
})
export class SummarizePaymentComponent implements OnInit {
  @Input() Reservation: any;
  @Input() car: any;
  @Input() Passenger_Info: any;
  @Input() Price_info: any;
  @Input() Payment_info: any;
  @Input() PickUpFromField: any;
  @Input() MeetGreet: any;
  @Output() GetData = new EventEmitter();
  @Output() Back = new EventEmitter();
     // Data Pick Up Type
     PickUpType = {
      PTP:'Point to Point',
      HR:'Hourly',
      DL:'Daily',
     };
     DataPayment = {};
     Book_info = [
       {name:'OtherNotes',label:'Other notes',value:'I have a pet riding with me'},
       {name:'Stops',label:'Stops/EstTime',value:'45 Rockefeller Plaza, New York, NY 10111 (20 minutes)'},
       {name:'BookerName',label:'Booker name',value:'Booker Name'},
       {name:'IATANAccount',label:'IATAN # / Account #',value:'#123456'},
       {name:'EmailAddress',label:'Email Address',value:'sample@gmail.com'},
       {name:'ContactNumber',label:'Contact Number',value:'+1 234-567-8910'},
       {name:'RewardsOptional',label:'Rewards (Optional)',value:'United Mileage Account'},
       {name:'Comments',label:'Comments',value:"Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."},
     ];
     show_review =  true;
     Check_form = false;
     contact = 'Your credit card will be authorized now, but  <b>NOT</b> charged until the ride is complete. Once you receive your confirmation number you can always manage your reservations through our website or by calling our office at <b>1-800-266-5254</b>  or for international <b>+1-212-213-0302</b>.  Promo code does not automatically apply for commission accounts, please forward your reservation confirmation to <b>dispatch@bookalimo.com</b> and include your promotion code to apply the discount.';
  constructor() { }

  ngOnInit(): void {
   this.Book_info =  this.data_Book(this.Book_info);
   this.Price_info =  this.data_price(this.Price_info);
  }

  show_collase(){
    this.show_review = !this.show_review;
  }

  UpdateBack(s){
    this.Back.emit(s);
  }

  check_forms(){
    this.Check_form = true;
    const error = [];
    const check = [
    'holder_name',
    'billing_zip',
    'expires',
    ];
    for(var i of check){
      if(!this.Payment_info[i])error.push(i);
    }
    if(this.Payment_info['card'].length < 15 || this.Payment_info['card'].length > 16){
      error.push('card');
    }
    if(this.Payment_info['expires'] < 1 || this.Payment_info['expires'] > 12){
      error.push('expires');
    }
    if(this.Payment_info['cvv'].length !=3){
      error.push('cvv');
    }
    console.log(error);
    if(error.length<1){
      this.Payment_info['step_next'] = 5;
      this.GetData.emit(this.Payment_info);
    }
  }

  show_supports(data){
    var resulf = '';
    if(data)data.forEach(element => {
      if(element.checked)resulf += element.label +'<br/>';
    });
    return resulf;
  }
  show_stops(data){
    var resulf = '';
    if(data)data.forEach(element => {
     resulf += element.address+ ' ('+ element.EstMinutes +' minutes)' +'<br/>';
    });
    return resulf;
  }

  data_Book(data){
    var resulf = [];
   data.forEach(element => {
     // Book
     if(element.name =='OtherNotes')element.value= this.show_supports(this.Reservation.Supports);
     if(element.name =='Stops')element.value= this.show_stops(this.Reservation.AddStops);
     if(element.name =='BookerName')element.value= this.Passenger_Info.FullName;
     if(element.name =='IATANAccount')element.value= this.Passenger_Info.IATANAccount;
     if(element.name =='EmailAddress')element.value= this.Passenger_Info.EmailAddress;
     if(element.name =='ContactNumber')element.value= this.Passenger_Info.ContactNumber;
     if(element.name =='RewardsOptional')element.value= this.Passenger_Info.UnitedMileageAccount;
     if(element.name =='Comments')element.value= this.Passenger_Info.RewardsOptional;
      resulf.push(element);
   });
   return resulf;
  }

  data_price(data){
    var resulf = [];
    var total = 0;
    data.forEach(element => {
      if(element.name =='Total')element.value= total;   
      if(element.name =='BasicFare'){
        if(this.Reservation.PickUpType =='HR')element.value = this.car.hours * this.Reservation.PickUpTypeHours;  
      } 
      if(element.name =='MeetGreet' && this.Reservation.PickUpFrom =='airport')element.value= this.MeetGreet.fee; 
      if(element.name =='Parking' && this.Reservation.PickUpFrom =='airport' )element.value= this.MeetGreet.parking; 
      total += element.value;
      resulf.push(element);
      
    });
    return resulf;
  }

  ChangeEvent(name, event){
    this.Payment_info[name]=event.target.value
  }


}
