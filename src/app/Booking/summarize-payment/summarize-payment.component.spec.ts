import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SummarizePaymentComponent } from './summarize-payment.component';

describe('SummarizePaymentComponent', () => {
  let component: SummarizePaymentComponent;
  let fixture: ComponentFixture<SummarizePaymentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SummarizePaymentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SummarizePaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
