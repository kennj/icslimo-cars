import { Component, OnInit, Input, Output,  EventEmitter, HostListener } from '@angular/core';

@Component({
  selector: 'app-select-car-rates',
  templateUrl: './select-car-rates.component.html',
  styleUrls: ['./select-car-rates.component.scss']
})
export class SelectCarRatesComponent implements OnInit {
  @Input() Reservation: any;
  @Input() car: any;
  @Input() Select_cars: any;
  @Input() MeetGreet: any;
  @Input() Select_MeetGreet_choose: any;
  @Output() GetDataAirport = new EventEmitter();
  @Output() GetData = new EventEmitter();
  @Output() Back = new EventEmitter();
  Date = new Date();
    // Support
    select_choose =   0;
    select_img='/assets/images/sedan.png';
    Select_item:any;
   
    Select_MeetGreet_item:any;
  constructor() { }

  ngOnInit(): void {
    this.select_choose = this.car;
  this.Select_item = this.Select_cars[this.car];
  }

  SelectEvent(s){
    this.select_choose = s;
    this.select_img = this.Select_cars[s].img;
    this.Select_item = this.Select_cars[s];
  }
  SelectMeetGreet(m){
    this.Select_MeetGreet_choose = m;
    this.Select_MeetGreet_item = this.MeetGreet[m];
  }
  check_forms(){
    this.GetData.emit(this.select_choose);
    this.GetDataAirport.emit(this.Select_MeetGreet_choose);
  }

  UpdateBack(s){
    this.Back.emit(s);
  }

}
