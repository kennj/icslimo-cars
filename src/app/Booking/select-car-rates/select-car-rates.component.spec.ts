import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectCarRatesComponent } from './select-car-rates.component';

describe('SelectCarRatesComponent', () => {
  let component: SelectCarRatesComponent;
  let fixture: ComponentFixture<SelectCarRatesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectCarRatesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectCarRatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
