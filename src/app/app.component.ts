import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  Title = 'cars';
  Step = 1;
  /// New Reservation
  Reservation = [];
     // Pick Up From Data
     PickUpFromData = [
      {name:'address',label:'Address'},
      {name:'airport',label:'Airport'},
      {name:'train',label:'Train'},
      {name:'cruise',label:'Cruise'},
    ];
    PickUpFromField={
      From:{
        address:[
          {name:'country',label:'Country',value:'US - UNITED STATES',required:true,type:'countries'},
          {name:'city',label:'City',value:'New York',required:true,type:'city'},
          {name:'fulladdress',label:'Address',value:'123 street',required:true},
          {name:'pickupon',label:'Pick up on',value:''},
      ],
      airport:[
        {name:'airline',label:'Airline',value:'01 - AIRLINE',required:true,type:'airlines'},
        {name:'FlightNumber',label:'Flight Number',value:'123',required:true},
        {name:'OriginatingCity',label:'Originating city',value:'123',required:true},
    ],
    cruise:[
      {name:'shipname',label:'Ship name',value:'',required:true},
      {name:'pickupon',label:'Pick up on',value:''},
    ],
    train:[
      {name:'ArrivingFrom',label:'Arriving From',value:'',required:true,type:'countries'},
      {name:'TrainNumber',label:'Train number',value:'',required:true},
      {name:'pickupon',label:'Pick up on (optional)',value:''},
    ],
      },
      To: {
        address:[
          {name:'country',label:'Country',value:'US - UNITED STATES',required:true,type:'countries'},
          {name:'city',label:'City',value:'New York',required:true,type:'city'},
          {name:'fulladdress',label:'Address',value:'123 street',required:true},
          {name:'pickupon',label:'Pick up on',value:''},
      ],
      airport:[
        {name:'airline',label:'Airline',value:'01 - AIRLINE',required:true,type:'airlines'},
        {name:'FlightNumber',label:'Flight Number',value:'123',required:true},
        {name:'OriginatingCity',label:'Originating city',value:'123',required:true},
    ],
    cruise:[
      {name:'shipname',label:'Ship name',value:'',required:true},
      {name:'pickupon',label:'Pick up on',value:''},
    ],
    train:[
      {name:'ArrivingFrom',label:'Arriving From',value:'',required:true,type:'countries'},
      {name:'TrainNumber',label:'Train number',value:'',required:true},
      {name:'pickupon',label:'Pick up on (optional)',value:''},
    ],
      }
    }



/// In page 2 when Pick Up From = airport
  MeetGreet = [
    {option:'BaggageClaim', label:'Baggage Claim', fee: 11, parking: 5, instructions:'Meet your chauffeur by Baggage Claim even if you do not have any luggage' },
    {option:'CurbSide', label:'Curb Side', fee: 0, parking: 0, instructions:'*Please contact our Dispatch Center upon arrival. Phone: 1-800-266-5254 SMS: 1-212-561-2600 Whatsapp: 1-929-422-2227. We monitor the flight*' },
    {option:'Gate', label:'Gate', fee: 11, parking: 5, instructions:'Chauffeur will be waiting with the sign at the Gate Area' },
    {option:'International', label:'International', fee: 0, parking: 5, instructions:'Chauffeur will be waiting with the sign at the Outside Custom Area' },
  ];
  Select_MeetGreet_choose = 0;
/// List cars  
  Select_cars = [
    {name:'sedan',label:'SEDAN LINCOLNt, CADILLAC Or Similar',checked:false, img:'/assets/images/sedan.png',hours:55.55},
    {name:'van',label:'VAN 10 - 14 PASSENGER',checked:false, img:'/assets/images/van.png',hours:59},
    {name:'mercedes_s',label:'MERCEDES S CLASS',checked:false, img:'/assets/images/suv.png',hours:55},
    {name:'minibus_28',label:'MINIBUS 28-36 PASSENGERS',checked:false, img:'/assets/images/minibus.png',hours:69},
    {name:'suv',label:'SUV 5-6 PASSENGERS Or Similar',checked:false, img:'/assets/images/suv.png',hours:78},
    {name:'minibus_22',label:'MINIBUS 22-28 PASSENGERS',checked:false, img:'/assets/images/minibus.png',hours:78},
    {name:'sprint',label:'SPRINTER VAN 14 PASSENGERS Or Similar',checked:false, img:'/assets/images/sprinte.png',hours:89},
    {name:'motorcoach',label:'MOTORCOACH PREVOST',checked:false, img:'/assets/images/motor-coach.png',hours:88},
  ];
  car = 0;
  /// In page 3
  Passenger_Info = {
    FirstName:'Adam',
    LastName:'Sandler',
    EmailAddress:'test@gmail.com',
    ContactNumber:'+1 212-332-6868',
    IATANAccount:'123456789',
    FullName:'Adam Sandler',
    EmailAddressBooker:'test@gmail.com',
    ContactNumberBooker:'+1 212-332-6868',
    UnitedMileageAccount:'test',
    RewardsOptional:'test',
  };
  Price_info = [
    {name:'BasicFare',label:'Basic Fare',value:0},
    {name:'Gratuity',label:'Gratuity',value:400},
    {name:'Toll',label:'Toll',value:0},
    {name:'Parking',label:'Parking',value:0},
    {name:'MeetGreet',label:'Meet & Greet',value:0},
    {name:'Misc',label:'Misc',value:75},
    {name:'STCChange',label:'STC Change',value:329.28},
    {name:'WCTax',label:'WC Tax',value:0},
    {name:'Total',label:'Estimated Total',value:0},
  ];
  Payment_info = {
    card:'4242424242424242',
    expires:'12',
    cvv:'123',
    holder_name:'kenny',
    billing_zip:'8000',
    promo_code:'',
  };

  DataCar = {};

  constructor(

  ) { }

  ngOnInit() {

  }
  // get data step 1
  UpdateReservation(data){
    if(data.step_next == 2){
      this.Reservation = data;
      this.Step = 2;
    }

  }
/// button back step 
  UpdateBack(s){
    this.Step = s;
  }
/// get data setp 2
  UpdateCar(data){
    this.car =  data;
    this.Step = 3;
  }
  UpdateAirport(data){
    this.Select_MeetGreet_choose = data;
  }
/// get data step 3
  UpdateInfo(data){
    if(data.step_next == 4){
      this.Passenger_Info = data;
      this.Step = 4;
    }
  }
  UpdateFromField(data){
    this.PickUpFromField = data;
  }
/// get data step 4
  UpdatePayment(data){
    if(data.step_next == 5){
      this.Payment_info = data;
      this.Step = 5;
      this.DataCar['Reservation'] = this.Reservation;
      this.DataCar['PickUpFromField'] = this.PickUpFromField.From[this.Reservation['PickUpFrom']];
      this.DataCar['PickUpToField'] = this.PickUpFromField.To[this.Reservation['PickUpTo']];
      this.DataCar['Select_cars'] = this.Select_cars[this.car];
      this.DataCar['Passenger_Info'] = this.Passenger_Info;
      this.DataCar['Price_info'] = this.Price_info;
      this.DataCar['Payment_info'] = this.Payment_info;
      /// All data of customer
      console.log(this.DataCar);
    }
  }

}
