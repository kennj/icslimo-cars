var options = [
   {
      "code":"US",
      "label":"US - UNITED STATES"
   },
   {
      "code":"AE",
      "label":"AE - UAE"
   },
   {
      "code":"AF",
      "label":"AF - AFGHANISTAN"
   },
   {
      "code":"AG",
      "label":"AG - ANTIGUA AND BARBUDA"
   },
   {
      "code":"AI",
      "label":"AI - ANGUILLA"
   },
   {
      "code":"AL",
      "label":"AL - ALBANIA"
   },
   {
      "code":"AM",
      "label":"AM - ARMENIA"
   },
   {
      "code":"AN",
      "label":"AN - NETHERLANDS ANTILLES"
   },
   {
      "code":"AO",
      "label":"AO - ANGOLA"
   },
   {
      "code":"AQ",
      "label":"AQ - ANTARCTICA"
   },
   {
      "code":"AR",
      "label":"AR - ARGENTINA"
   },
   {
      "code":"AS",
      "label":"AS - AMERICAN SAMOA"
   },
   {
      "code":"AT",
      "label":"AT - AUSTRIA"
   },
   {
      "code":"AU",
      "label":"AU - AUSTRALIA"
   },
]