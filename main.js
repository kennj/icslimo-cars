var AppComponent = /** @class */ (function () {
    function AppComponent() {
        // Database
        this.Title = 'cars';
        this.Step = 1;
        /// New Reservation
        // Data Pick Up Type
        this.PickUpType = [
            { name: 'PTP', option: 'Point to Point' },
            { name: 'HR', option: 'Hourly', value: '', required: true },
            { name: 'DL', option: 'Daily' },
        ];
        this.selectType = 0;
        // Support
        this.Supports = [
            { name: 'pet', label: 'I have a pet riding with me', checked: false },
            { name: 'booster', label: 'I need a booster', checked: false },
            { name: 'car_seat', label: 'I need a car seat', checked: false },
            { name: 'infant', label: 'I need an infant car seat', checked: false },
        ];
        // Add Stops
        this.AddStops = [];
        this.Check_form = false;
        this.date = new Date();
        this.Reservation = {
            PickUpDate: this.date.toLocaleDateString(),
            PickUpTime: this.date.toLocaleTimeString('en-US', { hour: '2-digit', minute: '2-digit' }),
            PickUpFrom: 'address',
            PickUpTo: 'address',
            PickUpType: 'PTP',
            Luggage: 1,
            Passenger: 1,
            PickUpTypeHours: '',
            Supports: this.Supports,
            AddStops: this.AddStops
        };
        // Pick Up From Data
        this.PickUpFromData = [
            { name: 'address', label: 'Address' },
            { name: 'airport', label: 'Airport' },
            { name: 'train', label: 'Train' },
            { name: 'cruise', label: 'Cruise' },
        ];
        this.PickUpFromField = {
            From: {
                address: [
                    // {name:'country',label:'Country',value:'',required:true,type:'countries'},
                    { name: 'city', label: 'City', value: '', required: true, type: 'city' },
                    { name: 'addressfrom', label: 'Address', value: '', required: true },
                    { name: 'pickupon', label: 'Pick up on', value: '' },
                ],
                airport: [
                    { name: 'airport', label: 'Airport', value: '', required: true, type: 'airports' },
                    { name: 'airline', label: 'Airline', value: '', required: true, type: 'airlines' },
                    { name: 'Flight', label: 'Flight #', value: '', required: true },
                    { name: 'Origin', label: 'Origin', value: '', required: true },
                ],
                cruise: [
                    { name: 'postname', label: 'Post Name', value: '', required: true, type: 'seaports' },
                    { name: 'shipname', label: 'Ship name', value: '', required: true },
                    { name: 'pickupon', label: 'Pick up on', value: '' },
                ],
                train: [
                    { name: 'TrainStationFrom', label: 'Train Station', value: '', required: true },
                    { name: 'TrainNumber', label: 'Train number', value: '', required: true },
                    { name: 'pickupon', label: 'Pick up on (optional)', value: '' },
                ]
            },
            To: {
                address: [
                    // {name:'country',label:'Country',value:'',required:true,type:'countries'},
                    { name: 'city', label: 'City', value: '', required: true, type: 'city' },
                    { name: 'addressto', label: 'Address', value: '', required: true },
                    { name: 'Landmark', label: 'Landmark', value: '' },
                ],
                airport: [
                    { name: 'Airport', label: 'Airport', value: '', required: true, type: 'airports' },
                    { name: 'airline', label: 'Airline', value: '', required: true, type: 'airlines' },
                    { name: 'Flight', label: 'Flight #', value: '', required: true },
                    { name: 'FlyingTo', label: 'Flying To', value: '', required: true },
                ],
                cruise: [
                    { name: 'PortName', label: 'Port Name', value: '', required: true, type: 'seaports' },
                    { name: 'shipname', label: 'Ship name', value: '', required: true },
                ],
                train: [
                    { name: 'TrainStationTo', label: 'Train Station', value: '', required: true },
                    { name: 'TrainNumber', label: 'Train number', value: '', required: true },
                ]
            }
        };
        /// In page 2 when Pick Up From = airport
        this.MeetGreet = [
            { option: 'BaggageClaim', label: 'Baggage Claim', fee: 11, parking: 5, instructions: 'Meet your chauffeur by Baggage Claim even if you do not have any luggage' },
            { option: 'CurbSide', label: 'Curb Side', fee: 0, parking: 0, instructions: '*Please contact our Dispatch Center upon arrival. Phone: 1-800-266-5254 SMS: 1-212-561-2600 Whatsapp: 1-929-422-2227. We monitor the flight*' },
            { option: 'Gate', label: 'Gate', fee: 11, parking: 5, instructions: 'Chauffeur will be waiting with the sign at the Gate Area' },
            { option: 'International', label: 'International', fee: 0, parking: 5, instructions: 'Chauffeur will be waiting with the sign at the Outside Custom Area' },
        ];
        this.Select_MeetGreet_choose = 0;
        /// List cars  
        this.Select_cars = [
            { name: 'sedan', label: 'SEDAN LINCOLNt, CADILLAC Or Similar', checked: false, img: 'images/sedan.png', hours: 55.55 },
            { name: 'van', label: 'VAN 10 - 14 PASSENGER', checked: false, img: 'images/van.png', hours: 59 },
            { name: 'mercedes_s', label: 'MERCEDES S CLASS', checked: false, img: 'images/suv.png', hours: 55 },
            { name: 'minibus_28', label: 'MINIBUS 28-36 PASSENGERS', checked: false, img: 'images/minibus.png', hours: 69 },
            { name: 'suv', label: 'SUV 5-6 PASSENGERS Or Similar', checked: false, img: 'images/suv.png', hours: 78 },
            { name: 'minibus_22', label: 'MINIBUS 22-28 PASSENGERS', checked: false, img: 'images/minibus.png', hours: 78 },
            { name: 'sprint', label: 'SPRINTER VAN 14 PASSENGERS Or Similar', checked: false, img: 'images/sprinte.png', hours: 89 },
            { name: 'motorcoach', label: 'MOTORCOACH PREVOST', checked: false, img: 'images/motor-coach.png', hours: 88 },
        ];
        this.car = 0;
        /// In page 3
        this.Passenger_Info = {
            FirstName: '',
            LastName: '',
            EmailAddress: '',
            ContactNumber: '',
            IATANAccount: '',
            FullName: '',
            EmailAddressBooker: '',
            ContactNumberBooker: '',
            UnitedMileageAccount: '',
            RewardsOptional: ''
        };
        this.Book_info = [
            { name: 'OtherNotes', label: 'Other notes', value: 'I have a pet riding with me' },
            { name: 'Stops', label: 'Stops/EstTime', value: '' },
            { name: 'BookerName', label: 'Booker name', value: '' },
            { name: 'IATANAccount', label: 'IATAN # / Account #', value: '' },
            { name: 'EmailAddress', label: 'Email Address', value: '' },
            { name: 'ContactNumber', label: 'Contact Number', value: '' },
            { name: 'RewardsOptional', label: 'Rewards (Optional)', value: '' },
            { name: 'Comments', label: 'Comments', value: "Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua." },
        ];
        this.Price_info = [
            { name: 'BasicFare', label: 'Basic Fare', value: 0 },
            { name: 'Gratuity', label: 'Gratuity', value: 400 },
            { name: 'Toll', label: 'Toll', value: 0 },
            { name: 'Parking', label: 'Parking', value: 0 },
            { name: 'MeetGreet', label: 'Meet & Greet', value: 0 },
            { name: 'Misc', label: 'Misc', value: 75 },
            { name: 'STCChange', label: 'STC Change', value: 329.28 },
            { name: 'WCTax', label: 'WC Tax', value: 0 },
            { name: 'Total', label: 'Estimated Total', value: 0 },
        ];
        this.Payment_info = {
            card: '',
            expires: '',
            cvv: '',
            holder_name: '',
            billing_zip: '',
            promo_code: ''
        };
        this.DataCar = {};
        this.show_review = true;
    }
    // Save data step
    // get data step 1
    AppComponent.prototype.UpdateReservation = function (data) {
        if (data.step_next == 2) {
            this.Reservation = data;
            this.Step = 2;
        }
    };
    /// button back step 
    AppComponent.prototype.UpdateBack = function (s) {
        this.Step = s;
        setupall();
        window.scrollTo(0, 0);
    };
    /// get data setp 2
    AppComponent.prototype.UpdateCar = function (data) {
        this.car = data;
        this.Step = 3;
    };
    AppComponent.prototype.UpdateAirport = function (data) {
        this.Select_MeetGreet_choose = data;
    };
    /// get data step 3
    AppComponent.prototype.UpdateInfo = function (data) {
        if (data.step_next == 4) {
            this.Passenger_Info = data;
            this.Step = 4;
        }
    };
    AppComponent.prototype.UpdateFromField = function (data) {
        this.PickUpFromField = data;
    };
    /// get data step 4
    AppComponent.prototype.UpdatePayment = function (data) {
        if (data.step_next == 5) {
            this.Payment_info = data;
            this.Step = 5;
            this.DataCar['Reservation'] = this.Reservation;
            this.DataCar['PickUpFromField'] = this.PickUpFromField.From[this.Reservation['PickUpFrom']];
            this.DataCar['PickUpToField'] = this.PickUpFromField.To[this.Reservation['PickUpTo']];
            this.DataCar['Select_cars'] = this.Select_cars[this.car];
            this.DataCar['Passenger_Info'] = this.Passenger_Info;
            this.DataCar['Price_info'] = this.Price_info;
            this.DataCar['Payment_info'] = this.Payment_info;
            /// All data of customer
            console.log(this.DataCar);
        }
    };
    /// new reversation
    // save field when change
    AppComponent.prototype.DateEvent = function (name, event) {
        this.Reservation[name] = event.value;
        if (event.value == 'HR') {
            document.querySelector('[TypePickUpTypeHours]').className = 'mdl-cell mdl-cell--4-col';
        }
        else {
            document.querySelector('[TypePickUpTypeHours]').className = 'mdl-cell mdl-cell--4-col mdl-none';
        }
    };
    AppComponent.prototype.InputEvent = function (name, event) {
        this.Reservation[name] = event.value;
    };
    AppComponent.prototype.SupportEvent = function (i, name, event) {
        this.Reservation.Supports[i]['checked'] = event.checked;
    };
    AppComponent.prototype.timeChanged = function (event) {
        this.Reservation['PickUpTime'] = event.value;
    };
    AppComponent.prototype.PickUpEvent = function (name, event) {
        this.Reservation[name] = event.target.value;
    };
    AppComponent.prototype.filter_data = function (data, key, value) {
        var result = data.filter(function (S) { return S[key].includes(value); });
        return (result) ? result[0] : '';
    };
    AppComponent.prototype.AddNewStops = function () {
        var item = { address: '', Route: false, EstMinutes: 20 };
        var count = this.AddStops.length;
        var ItemSupport = '<div class="mdl-grid mdl-mt-2"><div class="mdl-cell mdl-cell--6-col" > <label class="form-control-5">Address / Airport</label><div class="form-control-5 mdl-float-right mdl-text-right mh-4"> <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect R-width-auto" > <input type="checkbox" class="mdl-checkbox__input" onclick="app.AddStopEvent(' + count + ',`Route`,this)"> <span class="mdl-checkbox__label">En Route</span> </label></div><div class="form-group mdl-mt-2"> <input class="form-control" placeholder="Address / Airport" value="' + item.address + '" onchange="app.AddStopEvent(' + count + ',`address`,this)"></div></div><div class="mdl-cell mdl-cell--6-col" > <label class="form-control-5">Est. Minutes</label><div class="form-control-5 mdl-float-right mdl-text-right"> <button class="mat-icon-delete" onclick="app.DeleteStops(' + count + ')"> <span class="material-icons">delete</span> </button></div><div class="form-group mdl-mt-2"> <input type="number" class="form-control" min="20" value="' + item.EstMinutes + '" onchange="app.AddStopEvent(' + count + ',`EstMinutes`,this)"></div></div></div>';
        if (count < 3) {
            this.AddStops[count] = item;
            var div = document.createElement('div');
            div.innerHTML = ItemSupport;
            div.className = 'Support-item-' + count;
            document.querySelector('.support-data').appendChild(div);
        }
    };
    AppComponent.prototype.DeleteStops = function (s) {
        this.AddStops.splice(s, 1);
        document.querySelector('.Support-item-' + s).remove();
        document.querySelector('.support-data').innerHTML = '';
        this.AddStops.forEach(function (item, count) {
            var checked = (item.Route) ? 'checked' : '';
            var ItemSupport = '<div class="mdl-grid mdl-mt-2"><div class="mdl-cell mdl-cell--6-col" > <label class="form-control-5">Address / Airport</label><div class="form-control-5 mdl-float-right mdl-text-right mh-4"> <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect R-width-auto" > <input type="checkbox" class="mdl-checkbox__input" onclick="app.AddStopEvent(' + count + ',`Route`,this)" ' + checked + '> <span class="mdl-checkbox__label">En Route</span> </label></div><div class="form-group mdl-mt-2"> <input class="form-control" placeholder="Address / Airport" value="' + item.address + '" onchange="app.AddStopEvent(' + count + ',`address`,this)"></div></div><div class="mdl-cell mdl-cell--6-col" > <label class="form-control-5">Est. Minutes</label><div class="form-control-5 mdl-float-right mdl-text-right"> <button class="mat-icon-delete" onclick="app.DeleteStops(' + count + ')"> <span class="material-icons">delete</span> </button></div><div class="form-group mdl-mt-2"> <input type="number" class="form-control" min="20" value="' + item.EstMinutes + '" onchange="app.AddStopEvent(' + count + ',`EstMinutes`,this)"></div></div></div>';
            var div = document.createElement('div');
            div.innerHTML = ItemSupport;
            div.className = 'Support-item-' + count;
            document.querySelector('.support-data').appendChild(div);
        });
    };
    AppComponent.prototype.AddStopEvent = function (s, name, event) {
        this.AddStops[s][name] = event.value;
        if (name == 'Route')
            this.AddStops[s][name] = event.checked;
    };
    // step 2 select car
    AppComponent.prototype.setupMeetGreet = function () {
        var _this = this;
        var data = this.MeetGreet;
        var option = '';
        data.forEach(function (item, index) {
            var checked = (_this.Select_MeetGreet_choose == index) ? 'checked' : '';
            option += '<div class="mdl-cell mdl-cell--12-col"><div class="mdl-grid mdl-radio-MeetGreet"><div class="mdl-cell mdl-cell--2-col"> <label onclick="app.SelectMeetGreet(' + index + ')" class="mdl-js-radio mdl-js-ripple-effect form-control"> <input type="radio" class="mdl-radio__button" name="select_MeetGreet" value="' + item.option + '" ' + checked + '> <span class="mdl-radio__label">' + item.label + '</span> </label></div><div class="mdl-cell mdl-cell--2-col">' + formatter.format(item.fee) + '</div><div class="mdl-cell mdl-cell--2-col">' + formatter.format(item.parking) + '</div><div class="mdl-cell mdl-cell--6-col">' + item.instructions + '</div></div></div>';
        });
        document.querySelector('.show-Meet-Greet').innerHTML = option;
        if (this.Reservation['PickUpFrom'] == 'airport') {
            document.querySelector('[MeetGreetAirport]').className = 'mdl-grid bt-1';
        }
        else {
            document.querySelector('[MeetGreetAirport]').className = 'mdl-grid bt-1 mdl-none';
        }
        var label = '';
        this.Select_cars.forEach(function (item, index) {
            var checked = (_this.car == index) ? 'checked' : '';
            label += '<label class="mdl-radio mdl-js-radio mdl-js-ripple-effect form-control-5"  onclick="app.SelectEvent(' + index + ')"> <input type="radio" class="mdl-radio__button" name="select_car" value="' + item.name + '"  ' + checked + ' > <span class="mdl-radio__label">' + item.label + '</span> </label>';
        });
        document.querySelector('[ShowSelectCars]').innerHTML = label;
        document.querySelector('[ShowImageCars]').setAttribute('src', this.Select_cars[this.car].img);
        document.querySelector('[ShowPriceCar]').textContent = formatter.format(this.Select_cars[this.car].hours);
        document.querySelectorAll('[ShowPassenger]').forEach(function (item) { item.textContent = string_number(_this.Reservation.Passenger); });
        document.querySelectorAll('[ShowLuggage]').forEach(function (item) { item.textContent = string_number(_this.Reservation.Luggage); });
    };
    // end 
    AppComponent.prototype.SelectEvent = function (s) {
        this.car = s;
        this.setupMeetGreet();
    };
    AppComponent.prototype.SelectMeetGreet = function (m) {
        this.Select_MeetGreet_choose = m;
    };
    // step 3 Passenger Information
    AppComponent.prototype.SetupPassengerInfo = function () {
        var _this = this;
        // Pick Up From
        var option = '';
        this.PickUpFromField.From[this.Reservation.PickUpFrom].forEach(function (item, index) {
            var data = item.type ? item.type : '';
            var id = '';
            var display = 'mdl-none';
            if (item.name == 'addressfrom' || item.name == 'TrainStationFrom') {
                var id = 'id="input-' + item.name + '" onkeyup="searchgooglemap(`#input-' + item.name + '`)"';
            }
            if (_this.Check_form && !item.value && item.required) {
                var display = '';
            }
            var keyup = (data) ? 'onkeyup="app.autocomplete(' + data + ',' + index + ',`From`,`' + _this.Reservation.PickUpFrom + '`,this)"' : '';
            option += '<div class="ShowItemField"><input type="text" value="' + item.value + '" name="' + item.name + '" ' + id + ' placeholder="' + item.label + '" onchange="app.ChangeEvent(' + index + ',`From`,`' + _this.Reservation.PickUpFrom + '`,this)" class="tags form-control" ' + keyup + ' ><div ShowAutocomplete ></div><div class="error-field ' + display + '" PickUpField' + item.name + '>Field is required.</div></div>';
        });
        document.querySelector('[ShowFieldFrom]').innerHTML = option;
        // Pick up To
        var option = '';
        this.PickUpFromField.To[this.Reservation.PickUpTo].forEach(function (item, index) {
            var data = item.type ? item.type : '';
            var id = '';
            var display = 'mdl-none';
            if (item.name == 'addressto' || item.name == 'TrainStationTo') {
                var id = 'id="input-' + item.name + '" onkeyup="searchgooglemap(`#input-' + item.name + '`)"';
            }
            if (_this.Check_form && !item.value && item.required) {
                var display = '';
            }
            var keyup = (data) ? 'onkeyup="app.autocomplete(' + data + ',' + index + ',`To`,`' + _this.Reservation.PickUpTo + '`,this)"' : '';
            option += '<div class="ShowItemField"><input type="text" value="' + item.value + '" name="' + item.name + '" ' + id + ' placeholder="' + item.label + '" onchange="app.ChangeEvent(' + index + ',`To`,`' + _this.Reservation.PickUpTo + '`,this)" class="tags form-control" ' + keyup + ' ><div ShowAutocomplete ></div><div class="error-field ' + display + '" PickUpField' + item.name + '>Field is required.</div></div>';
        });
        document.querySelector('[ShowFieldTo]').innerHTML = option;
        // Pick Up Type
        var Type = this.PickUpType.filter(function (item) { return item.name.includes(_this.Reservation.PickUpType); });
        var PickUpType = '';
        if (Type[0].name == 'HR') {
            PickUpType = Type[0].option + '(' + string_number(this.Reservation.PickUpTypeHours) + ' hours)';
        }
        else {
            PickUpType = Type[0].option;
        }
        document.querySelectorAll('[PickUpType]').forEach(function (item) { item.textContent = String(PickUpType); });
        // Show data support
        var label = '';
        this.Reservation.Supports.forEach(function (item, index) {
            if (item.checked == true)
                label += '<label class="mdl-checkbox form-control-5"><span class="material-icons">check</span><span class="mdl-checkbox__label">' + item.label + '</span></label>';
        });
        document.querySelectorAll('[ShowDataSupport]').forEach(function (item) { item.innerHTML = String(label); });
        /// show data Passenger Information
        for (var p in this.Passenger_Info) {
            document.querySelector('[name="' + p + '"]').setAttribute('value', String(this.Passenger_Info[p]));
        }
    };
    AppComponent.prototype.ChangeEvent = function (s, type, name, event) {
        this.PickUpFromField[type][name][s].value = event.value;
    };
    AppComponent.prototype.ChangeEventPassenger = function (name, event) {
        this.Passenger_Info[name] = event.value;
    };
    // getlistaddress(dom){
    //   searchgooglemap(dom);
    // }
    // Step 4 Summarize and Payment
    AppComponent.prototype.SetupSummarizePayment = function () {
        var _this = this;
        // Field From
        var option = '';
        this.PickUpFromField.From[this.Reservation.PickUpFrom].forEach(function (field, index) {
            option += '<div class="mdl-content-normal">' + field.label + ': ' + field.value + '</div>';
        });
        document.querySelector('[ShowPickUpFromField]').innerHTML = option;
        // Field To
        var option = '';
        this.PickUpFromField.To[this.Reservation.PickUpTo].forEach(function (field, index) {
            option += '<div class="mdl-content-normal">' + field.label + ': ' + field.value + '</div>';
        });
        document.querySelector('[ShowPickUpToField]').innerHTML = option;
        // show Passenger_Info
        var Passenger_Info = [
            'FirstName',
            'LastName',
            'EmailAddress',
            'ContactNumber',
        ];
        Passenger_Info.forEach(function (Passenger) {
            document.querySelector('[ShowPassenger' + Passenger + ']').textContent = _this.Passenger_Info[Passenger];
        });
        //Car Type
        document.querySelector('[ShowCarType]').textContent = this.Select_cars[this.car].label;
        // Show Book Info
        var option = '';
        this.data_Book(this.Book_info).forEach(function (item, index) {
            var border = (index < 1) ? 'bt-1' : '';
            option += '<div class="mdl-grid ' + border + '"><div class="mdl-cell mdl-cell--6-col"><div class="mdl-content-normal"><label class="label-control">' + item.label + '</label></div></div><div class="mdl-cell mdl-cell--6-col"><div class="mdl-content-normal" >' + item.value + '</div></div></div>';
        });
        document.querySelector('[ShowBookInfo]').innerHTML = option;
        // Show price info
        var option = '';
        this.data_price(this.Price_info).forEach(function (item, index) {
            var border = (index < 1) ? 'bt-1' : '';
            var border2 = (item.name == 'Total') ? 'bt-1' : '';
            var bold = (item.name == 'Total') ? 'mdl-text-bold' : '';
            option += '<div class="mdl-grid ' + border + ' ' + border2 + '"><div class="mdl-cell mdl-cell--6-col"><div class="mdl-content-normal"><label class="label-control mdl-text-bold">' + item.label + '</label></div></div><div class="mdl-cell mdl-cell--6-col"><div class="mdl-content-normal ' + bold + '" >' + formatter.format(item.value) + '</div></div></div>';
        });
        document.querySelector('[ShowPriceInformation]').innerHTML = option;
        if (this.show_review) {
            document.querySelector('app-summarize-payment [collase_show]').className = 'collase-show';
            document.querySelector('app-summarize-payment [ShowTextCollase]').textContent = 'Hide';
            document.querySelector('app-summarize-payment [keyboard_arrow_up]').className = 'material-icons';
            document.querySelector('app-summarize-payment [keyboard_arrow_down]').className = 'material-icons mdl-none';
        }
        else {
            document.querySelector('app-summarize-payment [collase_show]').className = 'collase-show mdl-none';
            document.querySelector('app-summarize-payment [ShowTextCollase]').textContent = 'Show';
            document.querySelector('app-summarize-payment [keyboard_arrow_up]').className = 'material-icons mdl-none';
            document.querySelector('app-summarize-payment [keyboard_arrow_down]').className = 'material-icons';
        }
    };
    AppComponent.prototype.ChangePayment_info = function (name, event) {
        this.Payment_info[name] = event.value;
    };
    AppComponent.prototype.show_collase = function () {
        this.show_review = !this.show_review;
        setupall();
    };
    AppComponent.prototype.data_Book = function (data) {
        var _this = this;
        var resulf = [];
        data.forEach(function (element) {
            // Book
            if (element.name == 'OtherNotes')
                element.value = _this.show_supports(_this.Reservation.Supports);
            if (element.name == 'Stops')
                element.value = _this.show_stops(_this.Reservation.AddStops);
            if (element.name == 'BookerName')
                element.value = _this.Passenger_Info.FullName;
            if (element.name == 'IATANAccount')
                element.value = _this.Passenger_Info.IATANAccount;
            if (element.name == 'EmailAddress')
                element.value = _this.Passenger_Info.EmailAddress;
            if (element.name == 'ContactNumber')
                element.value = _this.Passenger_Info.ContactNumber;
            if (element.name == 'RewardsOptional')
                element.value = _this.Passenger_Info.UnitedMileageAccount;
            if (element.name == 'Comments')
                element.value = _this.Passenger_Info.RewardsOptional;
            resulf.push(element);
        });
        return resulf;
    };
    /// calculation price
    AppComponent.prototype.data_price = function (data) {
        var _this = this;
        var resulf = [];
        var total = 0;
        data.forEach(function (element) {
            if (element.name == 'Total')
                element.value = total;
            if (element.name == 'BasicFare') {
                if (_this.Reservation.PickUpType == 'HR')
                    element.value = _this.Select_cars[_this.car].hours * Number(_this.Reservation.PickUpTypeHours);
            }
            if (element.name == 'MeetGreet' && _this.Reservation.PickUpFrom == 'airport')
                element.value = _this.MeetGreet[_this.Select_MeetGreet_choose].fee;
            if (element.name == 'Parking' && _this.Reservation.PickUpFrom == 'airport')
                element.value = _this.MeetGreet[_this.Select_MeetGreet_choose].parking;
            total += element.value;
            resulf.push(element);
        });
        return resulf;
    };
    AppComponent.prototype.show_supports = function (data) {
        var resulf = '';
        if (data)
            data.forEach(function (element) {
                if (element.checked)
                    resulf += element.label + '<br/>';
            });
        return resulf;
    };
    AppComponent.prototype.show_stops = function (data) {
        var resulf = '';
        if (data)
            data.forEach(function (element) {
                resulf += element.address + ' (' + element.EstMinutes + ' minutes)' + '<br/>';
            });
        return resulf;
    };
    AppComponent.prototype.isEmail = function (email) {
        var emailRegexp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return emailRegexp.test(email);
    };
    AppComponent.prototype.autocomplete = function (data, s, type, name, event) {
        var resulf = data.filter(function (search) { return search.label.toLowerCase().includes(event.value.toLowerCase()); });
        var resulf_s = '<ul>';
        resulf.forEach(function (item, index) {
            resulf_s += '<li onclick="app.InputAutocomplete(' + s + ',`' + type + '`,`' + name + '`,this)">' + item.label + '</li>';
        });
        resulf_s += '</ul>';
        event.nextElementSibling.innerHTML = resulf_s;
        event.nextElementSibling.className = 'mdl-show';
    };
    AppComponent.prototype.InputAutocomplete = function (s, type, name, event) {
        var data = event.textContent;
        event.parentElement.parentElement.previousElementSibling.value = data;
        this.hideElement('[showautocomplete]');
        this.PickUpFromField[type][name][s].value = data;
    };
    AppComponent.prototype.hideElement = function (name) {
        document.querySelectorAll(name).forEach(function (item) {
            item.className = 'mdl-none';
        });
    };
    AppComponent.prototype.ShowautocompleteALL = function (type) {
        var display = '';
        var id = '';
        var keyup = 'onkeyup="app.autocompleteALL(airports,city,seaports,' + type + ',this)"';
        var option = '<div class="ShowItemField"><input type="text" value="' + type + '" name="' + type + '" ' + id + ' placeholder="' + type + '" onchange="app.ChangeEvent(' + type + ',this)" class="tags form-control" ' + keyup + ' ><div ShowAutocomplete ></div><div class="error-field ' + display + '" PickUpField' + type + '>Field is required.</div></div>';
    };
    AppComponent.prototype.InputAutocompleteALL = function (type, name, event) {
        var data = event.getAttribute('data-content');
        event.parentElement.parentElement.previousElementSibling.value = data;
        this.hideElement('[showautocomplete]');
        this.PickUpFromField[type][name][0].value = data;
        this.Reservation['PickUp' + type] = name;
        if (name == 'address')
            document.querySelector('.ShowTypePickUp' + type + 'Choose span').textContent = 'location_city';
        if (name == 'airport')
            document.querySelector('.ShowTypePickUp' + type + 'Choose span').textContent = 'local_airport';
        if (name == 'cruise')
            document.querySelector('.ShowTypePickUp' + type + 'Choose span').textContent = 'directions_boat';
        if (name == 'train')
            document.querySelector('.ShowTypePickUp' + type + 'Choose span').textContent = 'train';
    };
    AppComponent.prototype.check_forms = function (step) {
        this.Check_form = true;
        var error = [];
        // check step 1
        if (step == 1) {
            var check = [
                'PickUpDate',
                'PickUpTime',
            ];
            for (var i in check) {
                document.querySelector('[' + check[i] + ']').className = "error-field mdl-none";
                if (!this.Reservation[check[i]]) {
                    error.push(check[i]);
                    document.querySelector('[' + check[i] + ']').className = "error-field";
                }
            }
            if (!this.Reservation['PickUpTypeHours'] && this.Reservation['PickUpType'] == 'HR') {
                error.push('PickUpTypeHours');
                document.querySelector('[PickUpTypeHours]').className = "error-field";
            }
            else {
                document.querySelector('[PickUpTypeHours]').className = "error-field mdl-none";
            }
            console.log(error);
            this.Reservation['AddStops'] = this.AddStops;
            if (error.length < 1) {
                this.Step = 2;
                setupall();
            }
        }
        // end step 1
        if (step == 2) {
            this.Step = 3;
            setupall();
        }
        // end step 2
        if (step == 3) {
            var check = [
                'FirstName',
                'LastName',
                'ContactNumber',
                'EmailAddress',
            ];
            for (var i in check) {
                document.querySelector('[Passenger' + check[i] + ']').className = "error-field mdl-none";
                if (!this.Passenger_Info[check[i]]) {
                    error.push(check[i]);
                    document.querySelector('[Passenger' + check[i] + ']').className = "error-field";
                }
                if (!this.isEmail(this.Passenger_Info['EmailAddress'])) {
                    error.push('EmailAddress');
                    document.querySelector('[PassengerEmailAddress').className = "error-field";
                }
            }
            for (var f in this.PickUpFromField.From[this.Reservation.PickUpFrom]) {
                var test = this.PickUpFromField.From[this.Reservation.PickUpFrom][f];
                if (!test.value && test.required) {
                    error.push(test.name);
                }
            }
            for (var t in this.PickUpFromField.To[this.Reservation.PickUpTo]) {
                var test = this.PickUpFromField.To[this.Reservation.PickUpTo][t];
                if (!test.value && test.required) {
                    error.push(test.name);
                }
            }
            this.SetupPassengerInfo();
            console.log(error);
            if (error.length < 1) {
                this.Step = 4;
                setupall();
            }
        }
        // start step 4
        if (step == 4) {
            for (var Payment in this.Payment_info) {
                document.querySelector('[Payment' + Payment + ']').className = 'error-field mdl-none';
                if (!this.Payment_info[Payment] && Payment != 'promo_code') {
                    error.push(Payment);
                    document.querySelector('[Payment' + Payment + ']').className = 'error-field';
                }
            }
            console.log(error);
            if (error.length < 1) {
                this.Step = 5;
                setupall();
            }
        }
    };
    return AppComponent;
}());
var app = new AppComponent();
/// set up field all form in step
function setupall() {
    app.Check_form = false;
    // setup data default
    document.querySelector('input.datepick-id').setAttribute('value', app.Reservation.PickUpDate);
    document.querySelector('input.timepicker').setAttribute('value', app.Reservation.PickUpTime);
    document.querySelectorAll('[ShowPickupdate]').forEach(function (item) { item.textContent = new Date(app.Reservation.PickUpDate).toDateString(); });
    document.querySelectorAll('[ShowPickupTime]').forEach(function (item) { item.textContent = app.Reservation.PickUpTime; });
    // step control
    document.querySelectorAll('[step]').forEach(function (item) {
        var step = Number(item.getAttribute('step'));
        if (app.Step >= step) {
            item.className = 'is-active mdl-cell mdl-item-control mdl-typography--text-center';
        }
        else {
            item.className = 'mdl-cell mdl-item-control mdl-typography--text-center';
        }
    });
    // Step form
    document.querySelectorAll('[StepForm]').forEach(function (item) {
        var step = Number(item.getAttribute('StepForm'));
        if (app.Step == step) {
            item.className = 'active-form';
        }
        else {
            item.className = 'mdl-none';
        }
    });
    // step control line
    document.querySelector('[line-step]').className = 'mdl-line mdl-line-' + app.Step;
    app.setupMeetGreet();
    app.SetupPassengerInfo();
    app.SetupSummarizePayment();
    window.scrollTo(0, 0);
}
// Create our number formatter.
var formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD'
});
function string_number(number) {
    if (number < 10)
        return '0' + number;
    return String(number);
}
setupall();
