var countries = [{
    "code":"US",
    "label":"US - UNITED STATES"
 },
 {
    "code":"AE",
    "label":"AE - UAE"
 },
 {
    "code":"AF",
    "label":"AF - AFGHANISTAN"
 },
 {
    "code":"AG",
    "label":"AG - ANTIGUA AND BARBUDA"
 },
 {
    "code":"AI",
    "label":"AI - ANGUILLA"
 },
 {
    "code":"AL",
    "label":"AL - ALBANIA"
 },
 {
    "code":"AM",
    "label":"AM - ARMENIA"
 },
 {
    "code":"AN",
    "label":"AN - NETHERLANDS ANTILLES"
 },
 {
    "code":"AO",
    "label":"AO - ANGOLA"
 },
 {
    "code":"AQ",
    "label":"AQ - ANTARCTICA"
 },
 {
    "code":"AR",
    "label":"AR - ARGENTINA"
 },
 {
    "code":"AS",
    "label":"AS - AMERICAN SAMOA"
 },
 {
    "code":"AT",
    "label":"AT - AUSTRIA"
 },
 {
    "code":"AU",
    "label":"AU - AUSTRALIA"
 },
 {
    "code":"AW",
    "label":"AW - ARUBA"
 },
 {
    "code":"AZ",
    "label":"AZ - AZERBAIJAN"
 },
 {
    "code":"BA",
    "label":"BA - BOSNIA AND HERZEGOVINA"
 },
 {
    "code":"BB",
    "label":"BB - BARBADOS"
 },
 {
    "code":"BD",
    "label":"BD - BANGLADESH"
 },
 {
    "code":"BE",
    "label":"BE - BELGIUM"
 },
 {
    "code":"BF",
    "label":"BF - BURKINA FASO"
 },
 {
    "code":"BG",
    "label":"BG - BULGARIA"
 },
 {
    "code":"BH",
    "label":"BH - BAHRAIN"
 },
 {
    "code":"BI",
    "label":"BI - BURUNDI"
 },
 {
    "code":"BJ",
    "label":"BJ - BENIN"
 },
 {
    "code":"BM",
    "label":"BM - BERMUDA"
 },
 {
    "code":"BN",
    "label":"BN - BRUNEI DARUSSALAM"
 },
 {
    "code":"BO",
    "label":"BO - BOLIVIA"
 },
 {
    "code":"BR",
    "label":"BR - BRAZIL"
 },
 {
    "code":"BS",
    "label":"BS - BAHAMAS"
 },
 {
    "code":"BT",
    "label":"BT - BHUTAN"
 },
 {
    "code":"BV",
    "label":"BV - BOUVET ISLAND"
 },
 {
    "code":"BW",
    "label":"BW - BOTSWANA"
 },
 {
    "code":"BY",
    "label":"BY - BELARUS"
 },
 {
    "code":"BZ",
    "label":"BZ - BELIZE"
 },
 {
    "code":"CA",
    "label":"CA - CANADA"
 },
 {
    "code":"CD",
    "label":"CD - CONGO, THE DEMOCRATIC REPUBLIC OF THE"
 },
 {
    "code":"CF",
    "label":"CF - CENTRAL AFRICAN REPUBLIC"
 },
 {
    "code":"CG",
    "label":"CG - CONGO"
 },
 {
    "code":"CH",
    "label":"CH - SWITZERLAND"
 },
 {
    "code":"CI",
    "label":"CI - COTE D\u0027IVOIRE"
 },
 {
    "code":"CK",
    "label":"CK - COOK ISLANDS"
 },
 {
    "code":"CL",
    "label":"CL - CHILE"
 },
 {
    "code":"CM",
    "label":"CM - CAMEROON"
 },
 {
    "code":"CN",
    "label":"CN - CHINA"
 },
 {
    "code":"CO",
    "label":"CO - COLOMBIA"
 },
 {
    "code":"CR",
    "label":"CR - COSTA RICA"
 },
 {
    "code":"CS",
    "label":"CS - SERBIA AND MONTENEGRO"
 },
 {
    "code":"CU",
    "label":"CU - CUBA"
 },
 {
    "code":"CV",
    "label":"CV - CAPE VERDE"
 },
 {
    "code":"CY",
    "label":"CY - CYPRUS"
 },
 {
    "code":"CZ",
    "label":"CZ - CZECH REPUBLIC"
 },
 {
    "code":"DE",
    "label":"DE - GERMANY"
 },
 {
    "code":"DJ",
    "label":"DJ - DJIBOUTI"
 },
 {
    "code":"DK",
    "label":"DK - DENMARK"
 },
 {
    "code":"DM",
    "label":"DM - DOMINICA"
 },
 {
    "code":"DO",
    "label":"DO - DOMINICAN REPUBLIC"
 },
 {
    "code":"DZ",
    "label":"DZ - ALGERIA"
 },
 {
    "code":"EC",
    "label":"EC - ECUADOR"
 },
 {
    "code":"EE",
    "label":"EE - ESTONIA"
 },
 {
    "code":"EG",
    "label":"EG - EGYPT"
 },
 {
    "code":"ER",
    "label":"ER - ERITREA"
 },
 {
    "code":"ES",
    "label":"ES - SPAIN"
 },
 {
    "code":"ET",
    "label":"ET - ETHIOPIA"
 },
 {
    "code":"EU",
    "label":"EU - EUROPE"
 },
 {
    "code":"FI",
    "label":"FI - FINLAND"
 },
 {
    "code":"FJ",
    "label":"FJ - FIJI"
 },
 {
    "code":"FK",
    "label":"FK - FALKLAND ISLANDS (MALVINAS)"
 },
 {
    "code":"FM",
    "label":"FM - MICRONESIA, FEDERATED STATES OF"
 },
 {
    "code":"FO",
    "label":"FO - FAROE ISLANDS"
 },
 {
    "code":"FR",
    "label":"FR - FRANCE"
 },
 {
    "code":"GA",
    "label":"GA - GABON"
 },
 {
    "code":"GB",
    "label":"GB - UNITED KINGDOM"
 },
 {
    "code":"GD",
    "label":"GD - GRENADA"
 },
 {
    "code":"GE",
    "label":"GE - GEORGIA"
 },
 {
    "code":"GF",
    "label":"GF - FRENCH GUIANA"
 },
 {
    "code":"GH",
    "label":"GH - GHANA"
 },
 {
    "code":"GI",
    "label":"GI - GIBRALTAR"
 },
 {
    "code":"GL",
    "label":"GL - GREENLAND"
 },
 {
    "code":"GM",
    "label":"GM - GAMBIA"
 },
 {
    "code":"GN",
    "label":"GN - GUINEA"
 },
 {
    "code":"GP",
    "label":"GP - GUADELOUPE"
 },
 {
    "code":"GQ",
    "label":"GQ - EQUATORIAL GUINEA"
 },
 {
    "code":"GR",
    "label":"GR - GREECE"
 },
 {
    "code":"GT",
    "label":"GT - GUATEMALA"
 },
 {
    "code":"GU",
    "label":"GU - GUAM"
 },
 {
    "code":"GW",
    "label":"GW - GUINEA-BISSAU"
 },
 {
    "code":"GY",
    "label":"GY - GUYANA"
 },
 {
    "code":"HK",
    "label":"HK - HONG KONG"
 },
 {
    "code":"HM",
    "label":"HM - HEARD ISLAND AND MCDONALD ISLANDS"
 },
 {
    "code":"HN",
    "label":"HN - HONDURAS"
 },
 {
    "code":"HR",
    "label":"HR - CROATIA"
 },
 {
    "code":"HT",
    "label":"HT - HAITI"
 },
 {
    "code":"HU",
    "label":"HU - HUNGARY"
 },
 {
    "code":"ID",
    "label":"ID - INDONESIA"
 },
 {
    "code":"IE",
    "label":"IE - IRELAND"
 },
 {
    "code":"IL",
    "label":"IL - ISRAEL"
 },
 {
    "code":"IN",
    "label":"IN - INDIA"
 },
 {
    "code":"IO",
    "label":"IO - BRITISH INDIAN OCEAN TERRITORY"
 },
 {
    "code":"IQ",
    "label":"IQ - IRAQ"
 },
 {
    "code":"IR",
    "label":"IR - IRAN, ISLAMIC REPUBLIC OF"
 },
 {
    "code":"IS",
    "label":"IS - ICELAND"
 },
 {
    "code":"IT",
    "label":"IT - ITALY"
 },
 {
    "code":"JM",
    "label":"JM - JAMAICA"
 },
 {
    "code":"JO",
    "label":"JO - JORDAN"
 },
 {
    "code":"JP",
    "label":"JP - JAPAN"
 },
 {
    "code":"KE",
    "label":"KE - KENYA"
 },
 {
    "code":"KG",
    "label":"KG - KYRGYZSTAN"
 },
 {
    "code":"KH",
    "label":"KH - CAMBODIA"
 },
 {
    "code":"KI",
    "label":"KI - KIRIBATI"
 },
 {
    "code":"KM",
    "label":"KM - COMOROS"
 },
 {
    "code":"KN",
    "label":"KN - SAINT KITTS AND NEVIS"
 },
 {
    "code":"KP",
    "label":"KP - KOREA, DEMOCRATIC PEOPLE\u0027S REPUBLIC OF"
 },
 {
    "code":"KR",
    "label":"KR - KOREA, REPUBLIC OF"
 },
 {
    "code":"KW",
    "label":"KW - KUWAIT"
 },
 {
    "code":"KY",
    "label":"KY - CAYMAN ISLANDS"
 },
 {
    "code":"KZ",
    "label":"KZ - KAZAKSTAN"
 },
 {
    "code":"LA",
    "label":"LA - LAO PEOPLE\u0027S DEMOCRATIC REPUBLIC"
 },
 {
    "code":"LB",
    "label":"LB - LEBANON"
 },
 {
    "code":"LC",
    "label":"LC - SAINT LUCIA"
 },
 {
    "code":"LI",
    "label":"LI - LIECHTENSTEIN"
 },
 {
    "code":"LK",
    "label":"LK - SRI LANKA"
 },
 {
    "code":"LR",
    "label":"LR - LIBERIA"
 },
 {
    "code":"LS",
    "label":"LS - LESOTHO"
 },
 {
    "code":"LT",
    "label":"LT - LITHUANIA"
 },
 {
    "code":"LU",
    "label":"LU - LUXEMBOURG"
 },
 {
    "code":"LV",
    "label":"LV - LATVIA"
 },
 {
    "code":"LY",
    "label":"LY - LIBYAN ARAB JAMAHIRIYA"
 },
 {
    "code":"MA",
    "label":"MA - MOROCCO"
 },
 {
    "code":"MC",
    "label":"MC - MONACO"
 },
 {
    "code":"MD",
    "label":"MD - MOLDOVA, REPUBLIC OF"
 },
 {
    "code":"MG",
    "label":"MG - MADAGASCAR"
 },
 {
    "code":"MH",
    "label":"MH - MARSHALL ISLANDS"
 },
 {
    "code":"MK",
    "label":"MK - MACEDONIA"
 },
 {
    "code":"ML",
    "label":"ML - MALI"
 },
 {
    "code":"MM",
    "label":"MM - MYANMAR"
 },
 {
    "code":"MN",
    "label":"MN - MONGOLIA"
 },
 {
    "code":"MO",
    "label":"MO - MACAU"
 },
 {
    "code":"MP",
    "label":"MP - NORTHERN MARIANA ISLANDS"
 },
 {
    "code":"MQ",
    "label":"MQ - MARTINIQUE"
 },
 {
    "code":"MR",
    "label":"MR - MAURITANIA"
 },
 {
    "code":"MS",
    "label":"MS - MONTSERRAT"
 },
 {
    "code":"MT",
    "label":"MT - MALTA"
 },
 {
    "code":"MU",
    "label":"MU - MAURITIUS"
 },
 {
    "code":"MV",
    "label":"MV - MALDIVES"
 },
 {
    "code":"MW",
    "label":"MW - MALAWI"
 },
 {
    "code":"MX",
    "label":"MX - MEXICO"
 },
 {
    "code":"MY",
    "label":"MY - MALAYSIA"
 },
 {
    "code":"MZ",
    "label":"MZ - MOZAMBIQUE"
 },
 {
    "code":"NA",
    "label":"NA - NAMIBIA"
 },
 {
    "code":"NC",
    "label":"NC - NEW CALEDONIA"
 },
 {
    "code":"NE",
    "label":"NE - NIGER"
 },
 {
    "code":"NF",
    "label":"NF - NORFOLK ISLAND"
 },
 {
    "code":"NG",
    "label":"NG - NIGERIA"
 },
 {
    "code":"NI",
    "label":"NI - NICARAGUA"
 },
 {
    "code":"NL",
    "label":"NL - NETHERLANDS"
 },
 {
    "code":"NO",
    "label":"NO - NORWAY"
 },
 {
    "code":"NP",
    "label":"NP - NEPAL"
 },
 {
    "code":"NR",
    "label":"NR - NAURU"
 },
 {
    "code":"NU",
    "label":"NU - NIUE"
 },
 {
    "code":"NZ",
    "label":"NZ - NEW ZEALAND"
 },
 {
    "code":"OM",
    "label":"OM - OMAN"
 },
 {
    "code":"PA",
    "label":"PA - PANAMA"
 },
 {
    "code":"PE",
    "label":"PE - PERU"
 },
 {
    "code":"PF",
    "label":"PF - FRENCH POLYNESIA"
 },
 {
    "code":"PG",
    "label":"PG - PAPUA NEW GUINEA"
 },
 {
    "code":"PH",
    "label":"PH - PHILIPPINES"
 },
 {
    "code":"PK",
    "label":"PK - PAKISTAN"
 },
 {
    "code":"PL",
    "label":"PL - POLAND"
 },
 {
    "code":"PR",
    "label":"PR - PUERTO RICO"
 },
 {
    "code":"PS",
    "label":"PS - PALESTINIAN TERRITORY, OCCUPIED"
 },
 {
    "code":"PT",
    "label":"PT - PORTUGAL"
 },
 {
    "code":"PW",
    "label":"PW - PALAU"
 },
 {
    "code":"PY",
    "label":"PY - PARAGUAY"
 },
 {
    "code":"QA",
    "label":"QA - QATAR"
 },
 {
    "code":"RE",
    "label":"RE - REUNION"
 },
 {
    "code":"RO",
    "label":"RO - ROMANIA"
 },
 {
    "code":"RU",
    "label":"RU - RUSSIAN FEDERATION"
 },
 {
    "code":"RW",
    "label":"RW - RWANDA"
 },
 {
    "code":"SA",
    "label":"SA - SAUDI ARABIA"
 },
 {
    "code":"SB",
    "label":"SB - SOLOMON ISLANDS"
 },
 {
    "code":"SC",
    "label":"SC - SEYCHELLES"
 },
 {
    "code":"SD",
    "label":"SD - SUDAN"
 },
 {
    "code":"SE",
    "label":"SE - SWEDEN"
 },
 {
    "code":"SG",
    "label":"SG - SINGAPORE"
 },
 {
    "code":"SI",
    "label":"SI - SLOVENIA"
 },
 {
    "code":"SK",
    "label":"SK - SLOVAKIA"
 },
 {
    "code":"SL",
    "label":"SL - SIERRA LEONE"
 },
 {
    "code":"SM",
    "label":"SM - SAN MARINO"
 },
 {
    "code":"SN",
    "label":"SN - SENEGAL"
 },
 {
    "code":"SO",
    "label":"SO - SOMALIA"
 },
 {
    "code":"SR",
    "label":"SR - SURINAME"
 },
 {
    "code":"ST",
    "label":"ST - SAO TOME AND PRINCIPE"
 },
 {
    "code":"SV",
    "label":"SV - EL SALVADOR"
 },
 {
    "code":"SY",
    "label":"SY - SYRIAN ARAB REPUBLIC"
 },
 {
    "code":"SZ",
    "label":"SZ - SWAZILAND"
 },
 {
    "code":"TC",
    "label":"TC - TURKS AND CAICOS ISLANDS"
 },
 {
    "code":"TD",
    "label":"TD - CHAD"
 },
 {
    "code":"TF",
    "label":"TF - FRENCH SOUTHERN TERRITORIES"
 },
 {
    "code":"TG",
    "label":"TG - TOGO"
 },
 {
    "code":"TH",
    "label":"TH - THAILAND"
 },
 {
    "code":"TJ",
    "label":"TJ - TAJIKISTAN"
 },
 {
    "code":"TK",
    "label":"TK - TOKELAU"
 },
 {
    "code":"TM",
    "label":"TM - TURKMENISTAN"
 },
 {
    "code":"TN",
    "label":"TN - TUNISIA"
 },
 {
    "code":"TO",
    "label":"TO - TONGA"
 },
 {
    "code":"TR",
    "label":"TR - TURKEY"
 },
 {
    "code":"TT",
    "label":"TT - TRINIDAD AND TOBAGO"
 },
 {
    "code":"TV",
    "label":"TV - TUVALU"
 },
 {
    "code":"TW",
    "label":"TW - TAIWAN"
 },
 {
    "code":"TZ",
    "label":"TZ - TANZANIA, UNITED REPUBLIC OF"
 },
 {
    "code":"UA",
    "label":"UA - UKRAINE"
 },
 {
    "code":"UG",
    "label":"UG - UGANDA"
 },
 {
    "code":"UM",
    "label":"UM - UNITED STATES MINOR OUTLYING ISLANDS"
 },
 {
    "code":"AD",
    "label":"AD - ANDORRA"
 },
 {
    "code":"UY",
    "label":"UY - URUGUAY"
 },
 {
    "code":"UZ",
    "label":"UZ - UZBEKISTAN"
 },
 {
    "code":"VC",
    "label":"VC - SAINT VINCENT AND THE GRENADINES"
 },
 {
    "code":"VE",
    "label":"VE - VENEZUELA"
 },
 {
    "code":"VG",
    "label":"VG - VIRGIN ISLANDS, BRITISH"
 },
 {
    "code":"VI",
    "label":"VI - VIRGIN ISLANDS, U.S."
 },
 {
    "code":"VN",
    "label":"VN - VIETNAM"
 },
 {
    "code":"VU",
    "label":"VU - VANUATU"
 },
 {
    "code":"WF",
    "label":"WF - WALLIS AND FUTUNA"
 },
 {
    "code":"WS",
    "label":"WS - SAMOA"
 },
 {
    "code":"YE",
    "label":"YE - YEMEN"
 },
 {
    "code":"YT",
    "label":"YT - MAYOTTE"
 },
 {
    "code":"ZA",
    "label":"ZA - SOUTH AFRICA"
 },
 {
    "code":"ZM",
    "label":"ZM - ZAMBIA"
 },
 {
    "code":"ZW",
    "label":"ZW - ZIMBABWE"
 }
]