

function searchgooglemap(dom){
        // Create the search box and link it to the UI element.
        const input = document.querySelector(dom);
        const searchBox = new google.maps.places.SearchBox(input);
    
        let markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener("places_changed", () => {
          const places = searchBox.getPlaces();
          console.log(places[0]['formatted_address']);
          $(dom).change();
          if (places.length == 0) {
            return;
          }
          // Clear out the old markers.
          markers.forEach((marker) => {
            marker.setMap(null);
          });
          markers = [];
          // For each place, get the icon, name and location.
          const bounds = new google.maps.LatLngBounds();
          places.forEach((place) => {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            const icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25),
            };  
            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
        });
}


function getPlacePredictions(type,event) {
 // Create the search box and link it to the UI element.
 var 	autocompleteService = new google.maps.places.AutocompleteService();
 var resultCollection = [];
 var option = '<ul>';
 airports.filter(item=>item.label.toLowerCase().includes(event.value.toLowerCase())).splice(0, 5).forEach(data=>{
  option +=  '<li onclick="app.InputAutocompleteALL(`'+type+'`,`airport`,this)" data-content="'+data.label+'"><span class="material-icons">local_airport</span><span>'+data.label+'</span> <span class="location mdl-float-right">Airport<span></li>';
 })
 city.filter(item=>item.label.toLowerCase().includes(event.value.toLowerCase())).splice(0, 5).forEach(data=>{
  option +=  '<li onclick="app.InputAutocompleteALL(`'+type+'`,`address`,this)" data-content="'+data.label+'"><span class="material-icons">location_city</span><span>'+data.label+'</span> <span class="location mdl-float-right">Address<span></li>';
 })
 seaports.filter(item=>item.label.toLowerCase().includes(event.value.toLowerCase())).splice(0, 5).forEach(data=>{
  option +=  '<li onclick="app.InputAutocompleteALL(`'+type+'`,`cruise`,this)" data-content="'+data.label+'"><span class="material-icons">directions_boat</span><span>'+data.label+'</span> <span class="location mdl-float-right">Cruise<span></li>';
 })

 // train.
autocompleteService.getPlacePredictions({
      input: event.value?event.value:'',
    }, function (places, status) {
      if (status == google.maps.places.PlacesServiceStatus.OK) {
        resultCollection = resultCollection.concat($.map(places || [], function (item) {
          return {
            code: item.place_id,
            label: item.description,
            isairport: false,
            googleData: item
          };
        }).splice(0, 5));
        
      }
      resultCollection.forEach(data=>{
        option +=  '<li onclick="app.InputAutocompleteALL(`'+type+'`,`train`,this)" data-content="'+data.label+'"><span class="material-icons">train</span><span>'+data.label+'</span> <span class="location mdl-float-right">Train<span></li>';
       });
  option += '</ul>';
  event.nextElementSibling.innerHTML=option;
  event.nextElementSibling.className = 'searchALL mdl-show';
    });

 
}


// controls main with library
$('.timepicker').mdtimepicker();

$( ".datepick-id" ).datepicker({dateFormat: 'dd/mm/yy',minDate: 0});




 $(document).click(function(event) { 
    var $target = $(event.target);
    if(!$target.closest('[ShowAutocomplete]').length && 
    $('[ShowAutocomplete]').is(":visible")) {
     app.hideElement('[ShowAutocomplete]');
    }        
  });

