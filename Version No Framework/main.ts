class AppComponent {
  // Database
    Title = 'cars';
    Step = 1;
    /// New Reservation
 // Data Pick Up Type
 PickUpType = [
  {name:'PTP',option:'Point to Point'},
  {name:'HR',option:'Hourly',value:'',required:true},
  {name:'DL',option:'Daily'},
];
selectType = 0;

// Support
Supports = [
  {name:'pet',label:'I have a pet riding with me',checked:false},
  {name:'booster',label:'I need a booster',checked:false},
  {name:'car_seat',label:'I need a car seat',checked:false},
  {name:'infant',label:'I need an infant car seat',checked:false},
];

  // Add Stops
  AddStops = [];


Check_form = false;
date = new Date(); 


Reservation = {
PickUpDate: this.date.toLocaleDateString(),
PickUpTime:this.date.toLocaleTimeString('en-US',{hour: '2-digit', minute: '2-digit'}),
PickUpFrom:'address',
PickUpTo:'address',
PickUpType:'PTP',
Luggage: 1,
Passenger: 1,
PickUpTypeHours:'',
Supports:this.Supports,
AddStops:this.AddStops,
};

       // Pick Up From Data
       PickUpFromData = [
        {name:'address',label:'Address'},
        {name:'airport',label:'Airport'},
        {name:'train',label:'Train'},
        {name:'cruise',label:'Cruise'},
      ];
      PickUpFromField={
        From:{
          address:[
            // {name:'country',label:'Country',value:'',required:true,type:'countries'},
            {name:'city',label:'City',value:'',required:true,type:'city'},
            {name:'addressfrom',label:'Address',value:'',required:true},
            {name:'pickupon',label:'Pick up on',value:''},
        ],
          airport:[
          {name:'airport',label:'Airport',value:'',required:true,type:'airports'},
          {name:'airline',label:'Airline',value:'',required:true,type:'airlines'},
          {name:'Flight',label:'Flight #',value:'',required:true},
          {name:'Origin',label:'Origin',value:'',required:true},
      ],
          cruise:[
        {name:'postname',label:'Post Name',value:'',required:true,type:'seaports'},
        {name:'shipname',label:'Ship name',value:'',required:true},
        {name:'pickupon',label:'Pick up on',value:''},
      ],
          train:[
        {name:'TrainStationFrom',label:'Train Station',value:'',required:true},
        {name:'TrainNumber',label:'Train number',value:'',required:true},
        {name:'pickupon',label:'Pick up on (optional)',value:''},
      ],
        },
        To: {
          address:[
            // {name:'country',label:'Country',value:'',required:true,type:'countries'},
            {name:'city',label:'City',value:'',required:true,type:'city'},
            {name:'addressto',label:'Address',value:'',required:true},
            {name:'Landmark',label:'Landmark',value:''},
        ],
          airport:[
          {name:'Airport',label:'Airport',value:'',required:true,type:'airports'},
          {name:'airline',label:'Airline',value:'',required:true,type:'airlines'},
          {name:'Flight',label:'Flight #',value:'',required:true},
          {name:'FlyingTo',label:'Flying To',value:'',required:true},
      ],
      cruise:[
        {name:'PortName',label:'Port Name',value:'',required:true,type:'seaports'},
        {name:'shipname',label:'Ship name',value:'',required:true},
      ],
      train:[
        {name:'TrainStationTo',label:'Train Station',value:'',required:true},
        {name:'TrainNumber',label:'Train number',value:'',required:true},
      ],
        }
      }
  
  
  
  /// In page 2 when Pick Up From = airport
    MeetGreet = [
      {option:'BaggageClaim', label:'Baggage Claim', fee: 11, parking: 5, instructions:'Meet your chauffeur by Baggage Claim even if you do not have any luggage' },
      {option:'CurbSide', label:'Curb Side', fee: 0, parking: 0, instructions:'*Please contact our Dispatch Center upon arrival. Phone: 1-800-266-5254 SMS: 1-212-561-2600 Whatsapp: 1-929-422-2227. We monitor the flight*' },
      {option:'Gate', label:'Gate', fee: 11, parking: 5, instructions:'Chauffeur will be waiting with the sign at the Gate Area' },
      {option:'International', label:'International', fee: 0, parking: 5, instructions:'Chauffeur will be waiting with the sign at the Outside Custom Area' },
    ];
    Select_MeetGreet_choose = 0;
  /// List cars  
    Select_cars = [
      {name:'sedan',label:'SEDAN LINCOLNt, CADILLAC Or Similar',checked:false, img:'images/sedan.png',hours:55.55},
      {name:'van',label:'VAN 10 - 14 PASSENGER',checked:false, img:'images/van.png',hours:59},
      {name:'mercedes_s',label:'MERCEDES S CLASS',checked:false, img:'images/suv.png',hours:55},
      {name:'minibus_28',label:'MINIBUS 28-36 PASSENGERS',checked:false, img:'images/minibus.png',hours:69},
      {name:'suv',label:'SUV 5-6 PASSENGERS Or Similar',checked:false, img:'images/suv.png',hours:78},
      {name:'minibus_22',label:'MINIBUS 22-28 PASSENGERS',checked:false, img:'images/minibus.png',hours:78},
      {name:'sprint',label:'SPRINTER VAN 14 PASSENGERS Or Similar',checked:false, img:'images/sprinte.png',hours:89},
      {name:'motorcoach',label:'MOTORCOACH PREVOST',checked:false, img:'images/motor-coach.png',hours:88},
    ];
    car = 0;
    /// In page 3
    Passenger_Info = {
      FirstName:'',
      LastName:'',
      EmailAddress:'',
      ContactNumber:'',
      IATANAccount:'',
      FullName:'',
      EmailAddressBooker:'',
      ContactNumberBooker:'',
      UnitedMileageAccount:'',
      RewardsOptional:'',
    };
    Book_info = [
      {name:'OtherNotes',label:'Other notes',value:'I have a pet riding with me'},
      {name:'Stops',label:'Stops/EstTime',value:''},
      {name:'BookerName',label:'Booker name',value:''},
      {name:'IATANAccount',label:'IATAN # / Account #',value:''},
      {name:'EmailAddress',label:'Email Address',value:''},
      {name:'ContactNumber',label:'Contact Number',value:''},
      {name:'RewardsOptional',label:'Rewards (Optional)',value:''},
      {name:'Comments',label:'Comments',value:"Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."},
    ];
    Price_info = [
      {name:'BasicFare',label:'Basic Fare',value:0},
      {name:'Gratuity',label:'Gratuity',value:400},
      {name:'Toll',label:'Toll',value:0},
      {name:'Parking',label:'Parking',value:0},
      {name:'MeetGreet',label:'Meet & Greet',value:0},
      {name:'Misc',label:'Misc',value:75},
      {name:'STCChange',label:'STC Change',value:329.28},
      {name:'WCTax',label:'WC Tax',value:0},
      {name:'Total',label:'Estimated Total',value:0},
    ];
    Payment_info = {
      card:'',
      expires:'',
      cvv:'',
      holder_name:'',
      billing_zip:'',
      promo_code:'',
    };
    DataCar = {};
    show_review =  true;
    constructor(
  
    ) { 
    }
  
    // Save data step
    // get data step 1
    UpdateReservation(data){
      if(data.step_next == 2){
        this.Reservation = data;
        this.Step = 2;
      }
  
    }
  /// button back step 
    UpdateBack(s){
      this.Step = s;
      setupall();
      window.scrollTo(0, 0);
    }
  /// get data setp 2
    UpdateCar(data){
      this.car =  data;
      this.Step = 3;
    }
    UpdateAirport(data){
      this.Select_MeetGreet_choose = data;
    }
  /// get data step 3
    UpdateInfo(data){
      if(data.step_next == 4){
        this.Passenger_Info = data;
        this.Step = 4;
      }
    }
    UpdateFromField(data){
      this.PickUpFromField = data;
    }
  /// get data step 4
    UpdatePayment(data){
      if(data.step_next == 5){
        this.Payment_info = data;
        this.Step = 5;
        this.DataCar['Reservation'] = this.Reservation;
        this.DataCar['PickUpFromField'] = this.PickUpFromField.From[this.Reservation['PickUpFrom']];
        this.DataCar['PickUpToField'] = this.PickUpFromField.To[this.Reservation['PickUpTo']];
        this.DataCar['Select_cars'] = this.Select_cars[this.car];
        this.DataCar['Passenger_Info'] = this.Passenger_Info;
        this.DataCar['Price_info'] = this.Price_info;
        this.DataCar['Payment_info'] = this.Payment_info;
        /// All data of customer
        console.log(this.DataCar);
      }
    }

    /// new reversation
    // save field when change
   DateEvent( name:string, event) {
    this.Reservation[name]= event.value;
    if(event.value =='HR'){
      document.querySelector('[TypePickUpTypeHours]').className='mdl-cell mdl-cell--4-col';
    }else{
      document.querySelector('[TypePickUpTypeHours]').className='mdl-cell mdl-cell--4-col mdl-none';
    }
  }
  InputEvent( name:string, event) {
   this.Reservation[name]= event.value;
 }
 SupportEvent(i,name,event){
   this.Reservation.Supports[i]['checked']=event.checked;
 }
 timeChanged(event){
   this.Reservation['PickUpTime']= event.value;
 }
  PickUpEvent(name:string, event){
   this.Reservation[name]= event.target.value;
  }


  filter_data(data,key,value) {
   const result = data.filter(S => S[key].includes(value));
    return (result)?result[0]:'';
 }
 AddNewStops(){
  const item = {address:'',Route:false,EstMinutes:20};
  const count = this.AddStops.length;
  const ItemSupport = '<div class="mdl-grid mdl-mt-2"><div class="mdl-cell mdl-cell--6-col" > <label class="form-control-5">Address / Airport</label><div class="form-control-5 mdl-float-right mdl-text-right mh-4"> <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect R-width-auto" > <input type="checkbox" class="mdl-checkbox__input" onclick="app.AddStopEvent('+count+',`Route`,this)"> <span class="mdl-checkbox__label">En Route</span> </label></div><div class="form-group mdl-mt-2"> <input class="form-control" placeholder="Address / Airport" value="'+item.address+'" onchange="app.AddStopEvent('+count+',`address`,this)"></div></div><div class="mdl-cell mdl-cell--6-col" > <label class="form-control-5">Est. Minutes</label><div class="form-control-5 mdl-float-right mdl-text-right"> <button class="mat-icon-delete" onclick="app.DeleteStops('+count+')"> <span class="material-icons">delete</span> </button></div><div class="form-group mdl-mt-2"> <input type="number" class="form-control" min="20" value="'+item.EstMinutes+'" onchange="app.AddStopEvent('+count+',`EstMinutes`,this)"></div></div></div>';
  if(count < 3){
    this.AddStops[count] = item;
    const div =  document.createElement('div');
    div.innerHTML = ItemSupport;
    div.className = 'Support-item-'+count;
   document.querySelector('.support-data').appendChild(div);
   }
}

 DeleteStops(s){
  this.AddStops.splice(s,1);
  document.querySelector('.Support-item-'+s).remove();
  document.querySelector('.support-data').innerHTML = '';
  this.AddStops.forEach((item, count)=>{
    const checked = (item.Route)?'checked':'';
    const ItemSupport = '<div class="mdl-grid mdl-mt-2"><div class="mdl-cell mdl-cell--6-col" > <label class="form-control-5">Address / Airport</label><div class="form-control-5 mdl-float-right mdl-text-right mh-4"> <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect R-width-auto" > <input type="checkbox" class="mdl-checkbox__input" onclick="app.AddStopEvent('+count+',`Route`,this)" '+checked+'> <span class="mdl-checkbox__label">En Route</span> </label></div><div class="form-group mdl-mt-2"> <input class="form-control" placeholder="Address / Airport" value="'+item.address+'" onchange="app.AddStopEvent('+count+',`address`,this)"></div></div><div class="mdl-cell mdl-cell--6-col" > <label class="form-control-5">Est. Minutes</label><div class="form-control-5 mdl-float-right mdl-text-right"> <button class="mat-icon-delete" onclick="app.DeleteStops('+count+')"> <span class="material-icons">delete</span> </button></div><div class="form-group mdl-mt-2"> <input type="number" class="form-control" min="20" value="'+item.EstMinutes+'" onchange="app.AddStopEvent('+count+',`EstMinutes`,this)"></div></div></div>';
    const div =  document.createElement('div');
    div.innerHTML = ItemSupport;
    div.className = 'Support-item-'+count;
    document.querySelector('.support-data').appendChild(div);
  });
 }
 AddStopEvent(s,name,event){
   this.AddStops[s][name] = event.value;
   if(name=='Route')this.AddStops[s][name]=event.checked;
 }
// step 2 select car
setupMeetGreet(){
  const data = this.MeetGreet;
  var option = '';
  data.forEach((item,index)=>{
    const checked = (this.Select_MeetGreet_choose == index)?'checked':'';
    option += '<div class="mdl-cell mdl-cell--12-col"><div class="mdl-grid mdl-radio-MeetGreet"><div class="mdl-cell mdl-cell--2-col"> <label onclick="app.SelectMeetGreet('+index+')" class="mdl-js-radio mdl-js-ripple-effect form-control"> <input type="radio" class="mdl-radio__button" name="select_MeetGreet" value="'+item.option+'" '+checked+'> <span class="mdl-radio__label">'+item.label+'</span> </label></div><div class="mdl-cell mdl-cell--2-col">'+formatter.format(item.fee) +'</div><div class="mdl-cell mdl-cell--2-col">'+ formatter.format(item.parking) +'</div><div class="mdl-cell mdl-cell--6-col">'+item.instructions+'</div></div></div>';
  })
document.querySelector('.show-Meet-Greet').innerHTML = option;

if(this.Reservation['PickUpFrom'] =='airport'){
  document.querySelector('[MeetGreetAirport]').className='mdl-grid bt-1';
}else{
  document.querySelector('[MeetGreetAirport]').className='mdl-grid bt-1 mdl-none';
}

var label = '';
this.Select_cars.forEach( (item,index)=>{
  const checked = (this.car == index)?'checked':'';
  label += '<label class="mdl-radio mdl-js-radio mdl-js-ripple-effect form-control-5"  onclick="app.SelectEvent('+index+')"> <input type="radio" class="mdl-radio__button" name="select_car" value="'+item.name+'"  '+checked+' > <span class="mdl-radio__label">'+item.label +'</span> </label>';
} );

document.querySelector('[ShowSelectCars]').innerHTML = label;
document.querySelector('[ShowImageCars]').setAttribute('src',this.Select_cars[this.car].img);
document.querySelector('[ShowPriceCar]').textContent = formatter.format( this.Select_cars[this.car].hours );
document.querySelectorAll('[ShowPassenger]').forEach(item=>{item.textContent =  string_number (this.Reservation.Passenger)})
document.querySelectorAll('[ShowLuggage]').forEach(item=>{item.textContent =  string_number (this.Reservation.Luggage)});


}
// end 
SelectEvent(s){
  this.car = s;
  this.setupMeetGreet();
}
SelectMeetGreet(m){
  this.Select_MeetGreet_choose = m;
}
// step 3 Passenger Information
SetupPassengerInfo(){
  // Pick Up From
  var option = '';
  this.PickUpFromField.From[this.Reservation.PickUpFrom].forEach((item,index)=>{
    const data = item.type?item.type:''; 
    const id = '';
    var display = 'mdl-none';
    if(item.name == 'addressfrom' || item.name == 'TrainStationFrom'){
    const id = 'id="input-'+item.name+'" onkeyup="searchgooglemap(`#input-'+item.name+'`)"';
    }
    if(this.Check_form && !item.value && item.required){
      var display = '';
    }
    const keyup = (data)?'onkeyup="app.autocomplete('+data+','+index+',`From`,`'+this.Reservation.PickUpFrom+'`,this)"':'';
    option += '<div class="ShowItemField"><input type="text" value="'+item.value+'" name="'+item.name+'" '+id+' placeholder="'+item.label+'" onchange="app.ChangeEvent('+index+',`From`,`'+this.Reservation.PickUpFrom+'`,this)" class="tags form-control" '+keyup+' ><div ShowAutocomplete ></div><div class="error-field '+display+'" PickUpField'+item.name+'>Field is required.</div></div>';
  })
document.querySelector('[ShowFieldFrom]').innerHTML = option;

// Pick up To
var option = '';
  this.PickUpFromField.To[this.Reservation.PickUpTo].forEach((item,index)=>{
    const data = item.type?item.type:'';
    const id = '';
    var display = 'mdl-none';
    if(item.name == 'addressto' || item.name == 'TrainStationTo'){
      const id = 'id="input-'+item.name+'" onkeyup="searchgooglemap(`#input-'+item.name+'`)"';
      }
      if(this.Check_form && !item.value && item.required){
        var display = '';
      }
    const keyup = (data)?'onkeyup="app.autocomplete('+data+','+index+',`To`,`'+this.Reservation.PickUpTo+'`,this)"':'';
    option += '<div class="ShowItemField"><input type="text" value="'+item.value+'" name="'+item.name+'" '+id+' placeholder="'+item.label+'" onchange="app.ChangeEvent('+index+',`To`,`'+this.Reservation.PickUpTo+'`,this)" class="tags form-control" '+keyup+' ><div ShowAutocomplete ></div><div class="error-field '+display+'" PickUpField'+item.name+'>Field is required.</div></div>';
  })
document.querySelector('[ShowFieldTo]').innerHTML = option;
// Pick Up Type
const Type = this.PickUpType.filter(item => item.name.includes(this.Reservation.PickUpType) );

var PickUpType = '';
if(Type[0].name == 'HR'){
   PickUpType = Type[0].option +'('+string_number(this.Reservation.PickUpTypeHours)+' hours)';
}else{
   PickUpType = Type[0].option ;
}
document.querySelectorAll('[PickUpType]').forEach(item=>{item.textContent = String(PickUpType)});
// Show data support
var label =''
this.Reservation.Supports.forEach( (item,index)=>{
  if(item.checked == true)label += '<label class="mdl-checkbox form-control-5"><span class="material-icons">check</span><span class="mdl-checkbox__label">'+item.label+'</span></label>';
} )
document.querySelectorAll('[ShowDataSupport]').forEach(item=>{item.innerHTML = String(label)});

/// show data Passenger Information
for(var p in this.Passenger_Info){
   document.querySelector('[name="'+p+'"]').setAttribute('value',String( this.Passenger_Info[p] ));
}

}
ChangeEvent(s,type,name,event){
  this.PickUpFromField[type][name][s].value = event.value;
}
ChangeEventPassenger(name,event){
  this.Passenger_Info[name] = event.value;
}
// getlistaddress(dom){
//   searchgooglemap(dom);
// }

// Step 4 Summarize and Payment
SetupSummarizePayment(){
  // Field From
  var option = '';
  this.PickUpFromField.From[this.Reservation.PickUpFrom].forEach((field,index)=>{
    option+='<div class="mdl-content-normal">'+field.label+': '+field.value+'</div>';
  })
  document.querySelector('[ShowPickUpFromField]').innerHTML = option;
// Field To
  var option = '';
  this.PickUpFromField.To[this.Reservation.PickUpTo].forEach((field,index)=>{
    option+='<div class="mdl-content-normal">'+field.label+': '+field.value+'</div>';
  })
  document.querySelector('[ShowPickUpToField]').innerHTML = option;

  // show Passenger_Info
  var Passenger_Info = [
    'FirstName',
    'LastName',
    'EmailAddress',
    'ContactNumber',
  ];
  Passenger_Info.forEach(Passenger=>{
    document.querySelector('[ShowPassenger'+Passenger+']').textContent = this.Passenger_Info[Passenger];
  });
  //Car Type
  document.querySelector('[ShowCarType]').textContent = this.Select_cars[this.car].label;
// Show Book Info
var option = '';
  this.data_Book(this.Book_info).forEach( (item,index)=>{
    const border = (index < 1)?'bt-1':'';
option +='<div class="mdl-grid '+border+'"><div class="mdl-cell mdl-cell--6-col"><div class="mdl-content-normal"><label class="label-control">'+item.label +'</label></div></div><div class="mdl-cell mdl-cell--6-col"><div class="mdl-content-normal" >'+item.value+'</div></div></div>'
 } );
 document.querySelector('[ShowBookInfo]').innerHTML = option;

 // Show price info
 var option = '';
this.data_price(this.Price_info).forEach( (item,index)=>{
  const border = (index < 1)?'bt-1':'';
  const border2 = (item.name =='Total')?'bt-1':'';
  const bold = (item.name =='Total')?'mdl-text-bold':'';
option +='<div class="mdl-grid '+border+' '+border2+'"><div class="mdl-cell mdl-cell--6-col"><div class="mdl-content-normal"><label class="label-control mdl-text-bold">'+item.label+'</label></div></div><div class="mdl-cell mdl-cell--6-col"><div class="mdl-content-normal '+bold+'" >'+ formatter.format(item.value) +'</div></div></div>';
});
document.querySelector('[ShowPriceInformation]').innerHTML = option;

if(this.show_review){
  document.querySelector('app-summarize-payment [collase_show]').className = 'collase-show';
  document.querySelector('app-summarize-payment [ShowTextCollase]').textContent = 'Hide';
  document.querySelector('app-summarize-payment [keyboard_arrow_up]').className = 'material-icons';
  document.querySelector('app-summarize-payment [keyboard_arrow_down]').className = 'material-icons mdl-none';
}else{
  document.querySelector('app-summarize-payment [collase_show]').className = 'collase-show mdl-none';
  document.querySelector('app-summarize-payment [ShowTextCollase]').textContent = 'Show';
  document.querySelector('app-summarize-payment [keyboard_arrow_up]').className = 'material-icons mdl-none';
  document.querySelector('app-summarize-payment [keyboard_arrow_down]').className = 'material-icons';
}




}

ChangePayment_info(name,event){
this.Payment_info[name] = event.value;
}



show_collase(){
  this.show_review = !this.show_review;
  setupall();
}
data_Book(data){
  var resulf = [];
 data.forEach(element => {
   // Book
   if(element.name =='OtherNotes')element.value= this.show_supports(this.Reservation.Supports);
   if(element.name =='Stops')element.value= this.show_stops(this.Reservation.AddStops);
   if(element.name =='BookerName')element.value= this.Passenger_Info.FullName;
   if(element.name =='IATANAccount')element.value= this.Passenger_Info.IATANAccount;
   if(element.name =='EmailAddress')element.value= this.Passenger_Info.EmailAddress;
   if(element.name =='ContactNumber')element.value= this.Passenger_Info.ContactNumber;
   if(element.name =='RewardsOptional')element.value= this.Passenger_Info.UnitedMileageAccount;
   if(element.name =='Comments')element.value= this.Passenger_Info.RewardsOptional;
    resulf.push(element);
 });
 return resulf;
}
/// calculation price
data_price(data){
  var resulf = [];
  var total = 0;
  data.forEach(element => {
    if(element.name =='Total')element.value= total;   
    if(element.name =='BasicFare'){
      if(this.Reservation.PickUpType =='HR')element.value = this.Select_cars[this.car].hours * Number( this.Reservation.PickUpTypeHours);  
    } 
    if(element.name =='MeetGreet' && this.Reservation.PickUpFrom =='airport')element.value= this.MeetGreet[this.Select_MeetGreet_choose].fee; 
    if(element.name =='Parking' && this.Reservation.PickUpFrom =='airport' )element.value= this.MeetGreet[this.Select_MeetGreet_choose].parking; 
    total += element.value;
    resulf.push(element);
    
  });
  return resulf;
}
show_supports(data){
  var resulf = '';
  if(data)data.forEach(element => {
    if(element.checked)resulf += element.label +'<br/>';
  });
  return resulf;
}
show_stops(data){
  var resulf = '';
  if(data)data.forEach(element => {
   resulf += element.address+ ' ('+ element.EstMinutes +' minutes)' +'<br/>';
  });
  return resulf;
}



isEmail(email:string)
{
  const emailRegexp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return emailRegexp.test(email);
}

autocomplete(data,s,type,name,event){
 const resulf =  data.filter(search=>search.label.toLowerCase().includes(event.value.toLowerCase()));
 var resulf_s = '<ul>';
 resulf.forEach( (item,index) => {
   resulf_s +=  '<li onclick="app.InputAutocomplete('+s+',`'+type+'`,`'+name+'`,this)">'+item.label+'</li>';
 });
 resulf_s +=  '</ul>';
event.nextElementSibling.innerHTML=resulf_s;
event.nextElementSibling.className = 'mdl-show';
}
InputAutocomplete(s,type,name,event){
  const  data = event.textContent;
  event.parentElement.parentElement.previousElementSibling.value = data;
  this.hideElement('[showautocomplete]');
  this.PickUpFromField[type][name][s].value = data;
}
hideElement(name){
  document.querySelectorAll(name).forEach(item=>{
    item.className='mdl-none';
  })
}

ShowautocompleteALL(type){
  const display = '';
  const id = '';
  const keyup = 'onkeyup="app.autocompleteALL(airports,city,seaports,'+type+',this)"';
  const option = '<div class="ShowItemField"><input type="text" value="'+type+'" name="'+type+'" '+id+' placeholder="'+type+'" onchange="app.ChangeEvent('+type+',this)" class="tags form-control" '+keyup+' ><div ShowAutocomplete ></div><div class="error-field '+display+'" PickUpField'+type+'>Field is required.</div></div>';
}

 InputAutocompleteALL(type,name,event){
   const  data = event.getAttribute('data-content');
   event.parentElement.parentElement.previousElementSibling.value = data;
   this.hideElement('[showautocomplete]');
   this.PickUpFromField[type][name][0].value = data;
   this.Reservation['PickUp'+type] = name;
   if(name=='address')document.querySelector('.ShowTypePickUp'+type+'Choose span').textContent = 'location_city';
   if(name=='airport')document.querySelector('.ShowTypePickUp'+type+'Choose span').textContent = 'local_airport';
   if(name=='cruise')document.querySelector('.ShowTypePickUp'+type+'Choose span').textContent = 'directions_boat';
   if(name=='train')document.querySelector('.ShowTypePickUp'+type+'Choose span').textContent = 'train';
 }


 check_forms(step){
   this.Check_form = true;
   const error = [];
   // check step 1
   if(step==1){
    const check = [
      'PickUpDate', 
      'PickUpTime',
    ];
    for(var i in check){
      document.querySelector('['+check[i]+']').className="error-field mdl-none";
      if(!this.Reservation[ check[i] ]){
        error.push(check[i]);
        document.querySelector('['+check[i]+']').className="error-field";
      }
    }
    if( !this.Reservation['PickUpTypeHours']&& this.Reservation['PickUpType'] == 'HR'){
      error.push('PickUpTypeHours');
      document.querySelector('[PickUpTypeHours]').className="error-field";
    }else{
      document.querySelector('[PickUpTypeHours]').className="error-field mdl-none";
    }
    console.log(error);
    this.Reservation['AddStops'] = this.AddStops;
    if(error.length<1){
      this.Step = 2;
      setupall();
    }
   }
   // end step 1
   if(step==2){
    this.Step = 3;
    setupall();
   } 
   // end step 2

   if(step==3){
    const check = [
      'FirstName', 
      'LastName',
      'ContactNumber',
      'EmailAddress',
    ];
    for(var i in check){
      document.querySelector('[Passenger'+check[i]+']').className="error-field mdl-none";
      if(!this.Passenger_Info[ check[i] ]){
        error.push(check[i]);
        document.querySelector('[Passenger'+check[i]+']').className="error-field";
      }

      if( !this.isEmail( this.Passenger_Info['EmailAddress'] ) ){
        error.push('EmailAddress');
        document.querySelector('[PassengerEmailAddress').className="error-field";
      }
    }

    
    for(var f in this.PickUpFromField.From[this.Reservation.PickUpFrom]){
      var test = this.PickUpFromField.From[this.Reservation.PickUpFrom][f];
      if(!test.value && test.required ){
        error.push(test.name);
      }
    }
    for(var t in this.PickUpFromField.To[this.Reservation.PickUpTo]){
      var test = this.PickUpFromField.To[this.Reservation.PickUpTo][t];
      if(!test.value && test.required ){
        error.push(test.name);
      }
    }
    this.SetupPassengerInfo();
    console.log(error);
    if(error.length<1){
      this.Step = 4;
      setupall();
    }

   }
   
   // start step 4
   if(step == 4){
    for(var Payment in this.Payment_info){
      document.querySelector('[Payment'+Payment+']').className = 'error-field mdl-none';
      if(!this.Payment_info[Payment] && Payment != 'promo_code' ){
        error.push(Payment);
        document.querySelector('[Payment'+Payment+']').className = 'error-field';
      }
    }
    console.log(error);
    if(error.length<1){
      this.Step = 5;
      setupall();
    }

   }


  }


  
  }


  var app =  new AppComponent();
  /// set up field all form in step
    function setupall(){
      app.Check_form = false;
      // setup data default
      document.querySelector('input.datepick-id').setAttribute('value',app.Reservation.PickUpDate);
      document.querySelector('input.timepicker').setAttribute('value',app.Reservation.PickUpTime);
      document.querySelectorAll('[ShowPickupdate]').forEach(item=>{item.textContent = new Date(app.Reservation.PickUpDate).toDateString();})
      document.querySelectorAll('[ShowPickupTime]').forEach(item=>{item.textContent = app.Reservation.PickUpTime;})
      // step control
      document.querySelectorAll('[step]').forEach(item=>{
          const step = Number( item.getAttribute('step') );
          if(app.Step >= step){
              item.className='is-active mdl-cell mdl-item-control mdl-typography--text-center';
          }else{
            item.className='mdl-cell mdl-item-control mdl-typography--text-center';
          }
      });
      // Step form
      document.querySelectorAll('[StepForm]').forEach(item=>{
        const step = Number( item.getAttribute('StepForm') );
        if(app.Step == step){
            item.className='active-form';
        }else{
          item.className='mdl-none';
        }
    });
      // step control line
      document.querySelector('[line-step]').className = 'mdl-line mdl-line-'+app.Step;
      app.setupMeetGreet();
      app.SetupPassengerInfo();
      app.SetupSummarizePayment();
  
     
      window.scrollTo(0, 0);
    }
  
    // Create our number formatter.
  var formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
  
    // These options are needed to round to whole numbers if that's what you want.
    //minimumFractionDigits: 0,
    //maximumFractionDigits: 0,
  });
  
  function string_number(number){
    if(number <10) return '0'+number;
    return String(number);
  }
  setupall();



  
